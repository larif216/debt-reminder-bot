# **DebtReminder Bot**

*Advance Programming's* Final Project 2018/2019 term 2  
- [Early proposal](https://docs.google.com/document/d/1D7seJv5r4xcIQSidDcXP0DYfO79STil1AojZWSdBfAU/edit?usp=sharing)  
- Our bot specifications are in this 
[Presentation Slide](https://drive.google.com/drive/folders/158Jsp47sPL3IHrp7M0Q9qgn3N4-oBjOz?usp=sharing)

## Meet The Team

Gusti Ngurah Yama Adi Putra (1706979253) - 
**[Uci](https://gitlab.com/Uci)**  
Jeremia Delviero Hutagalung (1706039875) - 
**[jeremiadelviero](https://gitlab.com/jeremiadelviero)**  
Muhamad Lutfi Arif (1706979360) - 
**[larif216](https://gitlab.com/larif216)**  
Muhammad Fachry Nataprawira (1706039704) - 
**[ciferivalle](https://gitlab.com/ciferivalle)**  
Muhammad Rasyid Gatra Wijaya (1606875781) - 
**[mrasyidg](https://gitlab.com/mrasyidg)**  
Steven Kusuman (1706028676) - 
**[ipmankus](https://gitlab.com/ipmankus)**   

## Coverage & Pipeline  

[![pipeline status](https://gitlab.com/larif216/debt-reminder-bot/badges/master/pipeline.svg)](https://gitlab.com/larif216/debt-reminder-bot/commits/master)    
[![coverage report](https://gitlab.com/larif216/debt-reminder-bot/badges/master/coverage.svg)](https://gitlab.com/larif216/debt-reminder-bot/commits/master)    