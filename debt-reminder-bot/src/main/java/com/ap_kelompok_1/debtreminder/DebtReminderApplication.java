package com.ap_kelompok_1.debtreminder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class DebtReminderApplication {

    public static void main(String[] args) {
        SpringApplication.run(DebtReminderApplication.class, args);
    }
}
