package com.ap_kelompok_1.debtreminder.controller;

import com.ap_kelompok_1.debtreminder.controller.chat_source.ChatSource;
import com.ap_kelompok_1.debtreminder.handler.ApplicationService;
import com.ap_kelompok_1.debtreminder.handler.UniversalHandler;
import com.ap_kelompok_1.debtreminder.handler.state.State;
import com.ap_kelompok_1.debtreminder.handler.state.helper.StateHelper;
import com.ap_kelompok_1.debtreminder.repository.DebtRepository;
import com.ap_kelompok_1.debtreminder.repository.LineUserRepository;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.GroupSource;
import com.linecorp.bot.model.event.source.RoomSource;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import java.text.ParseException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
@LineMessageHandler
public class DebtReminderController {

    private static final String BLANK_PICTURE_URL =
        "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png";

    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Autowired
    private LineUserRepository lineUserRepository;

    @Autowired
    private DebtRepository debtRepository;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private UniversalHandler universalHandler;

    @Autowired
    private StateHelper stateHelper;

    @EventMapping
    public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent)
        throws ExecutionException, InterruptedException,
        ParseException {
        Source source = messageEvent.getSource();
        String replyToken = messageEvent.getReplyToken();

        String userName;
        String command = getCommand(messageEvent);
        ChatSource chatSource = getChatSource(source);
        String userId = source.getUserId();
        String displayName = getUserDisplayName(userId);
        String urlPicture = getUrlPicture(userId);
        String statusMessage = getStatusMessage(userId);
        State state = stateHelper.getUserState(userId);
        switch (command) {
            case "/profile":
                userName = lineUserRepository.findUserNameByUserId(userId);
                universalHandler.setResponses(
                    state.profile(urlPicture, displayName, userName, statusMessage, userId));
                applicationService.setHandler(universalHandler);
                break;
            case "/register":
                try {
                    userName = getUserName(messageEvent);
                    universalHandler.setResponses(state.register(userName, userId, displayName));
                    applicationService.setHandler(universalHandler);
                } catch (ArrayIndexOutOfBoundsException e) {
                    universalHandler.setResponses(state.register("", "", ""));
                    applicationService.setHandler(universalHandler);
                }
                break;
            case "/debt":
                String debteeUserName = getUserName(messageEvent);
                String debteeId = lineUserRepository.findUserIdByUserName(debteeUserName);
                universalHandler.setResponses(state.addDebtee(userId, debteeId));
                applicationService.setHandler(universalHandler);
                break;
            case "/showdebt":
                universalHandler.setResponses(state.showDebt(userId));
                applicationService.setHandler(universalHandler);
                break;
            case "/showclaim":
                universalHandler.setResponses(state.showClaim(userId));
                applicationService.setHandler(universalHandler);
                break;
            case "/bye":
                universalHandler.setResponses(state.bye(chatSource, userId));
                applicationService.setHandler(universalHandler);
                break;
            case "/help":
                universalHandler.setResponses(state.help(userId));
                applicationService.setHandler(universalHandler);
                break;
            case "/cancel":
                universalHandler.setResponses(state.cancel(userId));
                applicationService.setHandler(universalHandler);
                break;
            case "/cek-kurs":
                universalHandler.setResponses(state.kurs(userId));
                applicationService.setHandler(universalHandler);
                break;
            default:
                universalHandler.setResponses(state.others(command, userId));
                applicationService.setHandler(universalHandler);
                break;
        }
        List<Message> responseMessage = applicationService.getResponse();
        this.reply(replyToken, responseMessage);
    }

    @EventMapping
    public void handlePostbackEvent(PostbackEvent event) {
        String command = getCommandPostback(event);
        String replyToken = event.getReplyToken();

        Source source = event.getSource();
        String userId = source.getUserId();
        State state = stateHelper.getUserState(userId);
        Long debtId;
        switch (command) {
            case "remindDebt":
                debtId = getDebtId(event);
                universalHandler.setResponses(state.remindDebtor(debtId));
                applicationService.setHandler(universalHandler);
                break;
            case "remindAllDebt":
                universalHandler.setResponses(state.remindAllDebtor(userId));
                applicationService.setHandler(universalHandler);
                break;
            case "settleDebt":
                debtId = getDebtId(event);
                universalHandler.setResponses(state.settleDebt(debtId));
                applicationService.setHandler(universalHandler);
                break;
            case "settleAllDebt":
                universalHandler.setResponses(state.settleAllDebt(userId));
                applicationService.setHandler(universalHandler);
                break;
            case "acceptDebt":
                debtId = getDebtId(event);
                universalHandler.setResponses(state.accept(debtId));
                applicationService.setHandler(universalHandler);
                break;
            case "rejectDebt":
                debtId = getDebtId(event);
                universalHandler.setResponses(state.reject(debtId));
                applicationService.setHandler(universalHandler);
                break;
        }
        List<Message> responseMessage = applicationService.getResponse();
        this.reply(replyToken, responseMessage);
    }

    private void reply(String replyToken, List<Message> message) {
        try {
            lineMessagingClient
                .replyMessage(new ReplyMessage(replyToken, message))
                .get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    public String getUserName(MessageEvent<TextMessageContent> event) {
        return event.getMessage().getText().split(" ")[1];
    }

    public String getCommand(MessageEvent<TextMessageContent> event) {
        return event.getMessage().getText().split(" ")[0].toLowerCase();
    }

    public String getCommandPostback(PostbackEvent event) {
        return event.getPostbackContent().getData().split(" ")[0];
    }

    public Long getDebtId(PostbackEvent event) {
        return Long.parseLong(event.getPostbackContent().getData().split(" ")[1]);
    }

    public String getUrlPicture(String userId) throws InterruptedException, ExecutionException {
        String urlPicture = lineMessagingClient.getProfile(userId).get().getPictureUrl();
        if (urlPicture == null) {
            urlPicture = BLANK_PICTURE_URL;
        }
        return urlPicture;
    }

    public String getStatusMessage(String userId) throws ExecutionException, InterruptedException {
        return lineMessagingClient.getProfile(userId).get().getStatusMessage();
    }

    public String getUserDisplayName(String userId)
        throws ExecutionException, InterruptedException {
        return lineMessagingClient.getProfile(userId).get().getDisplayName();
    }

    public ChatSource getChatSource(Source source) {
        ChatSource chatSource = new ChatSource();
        if (source instanceof GroupSource) {
            chatSource.setId(((GroupSource) source).getGroupId());
            chatSource.setType(ChatSource.GROUP);
        } else if (source instanceof RoomSource) {
            chatSource.setId(((RoomSource) source).getRoomId());
            chatSource.setType(ChatSource.ROOM);
        } else {
            chatSource.setType(ChatSource.CHAT);
        }
        return chatSource;
    }
}
