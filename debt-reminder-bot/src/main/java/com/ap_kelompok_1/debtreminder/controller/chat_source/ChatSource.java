package com.ap_kelompok_1.debtreminder.controller.chat_source;

import lombok.Data;

// TODO : Pindahin ini ke tempat lain

@Data
public class ChatSource {
    public static final String GROUP = "GROUP";
    public static final String ROOM = "ROOM";
    public static final String CHAT = "CHAT";

    String id;
    String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
