package com.ap_kelompok_1.debtreminder.handler;

import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.Message;
import java.util.List;

public interface Handler {
    List<Message> getResponse();

    List<PushMessage> getPushMessages();
}
