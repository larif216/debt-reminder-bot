package com.ap_kelompok_1.debtreminder.handler;

import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.Message;
import java.util.List;

public interface Service {
    List<Message> getResponse();

    List<PushMessage> getPushMessages();

    void setHandler(Handler handler);
}
