package com.ap_kelompok_1.debtreminder.handler;

import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.Message;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class UniversalHandler implements Handler {
    private List<Message> responses;
    private List<PushMessage> pushMessages;

    public UniversalHandler() {
        this.responses = new ArrayList<>();
    }

    public void setResponses(List<Message> responses) {
        this.responses = responses;
    }

    @Override
    public List<Message> getResponse() {
        return this.responses;
    }

    @Override
    public List<PushMessage> getPushMessages() {
        return this.pushMessages;
    }

    public void setPushMessages(List<PushMessage> pushMessages) {
        this.pushMessages = pushMessages;
    }
}
