package com.ap_kelompok_1.debtreminder.handler.flex_message;

import com.ap_kelompok_1.debtreminder.model.Debt;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.*;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.util.function.Supplier;
import java.text.NumberFormat;
import java.util.Locale;

import static java.util.Arrays.asList;

public class AcceptRejectFlexMessage implements Supplier<FlexMessage> {
    private Debt debt;
    Locale localeID = new Locale("in", "ID");
    NumberFormat formatter = NumberFormat.getCurrencyInstance(localeID);

    public AcceptRejectFlexMessage(Debt debt) {
        this.debt = debt;
    }

    @Override
    public FlexMessage get() {
        Box bodyBlock = BodyBlock();
        Bubble bubble = Bubble.builder()
                .body(bodyBlock)
                .build();
        return new FlexMessage("Debt Confirmation", bubble);
    }

    private Box BodyBlock() {
        Separator separator = Separator.builder()
                .margin(FlexMarginSize.XL)
                .build();
        Text debtTitle = Text.builder()
                .text("Debt Confirmation")
                .weight(Text.TextWeight.BOLD)
                .size(FlexFontSize.XL)
                .margin(FlexMarginSize.MD)
                .build();
        Text idDebt = Text.builder()
                .size(FlexFontSize.SM)
                .text("Debt Id")
                .color("#555555")
                .flex(4)
                .build();
        Text debtor = Text.builder()
                .size(FlexFontSize.SM)
                .text("Debtor")
                .color("#555555")
                .flex(4)
                .build();
        Text nominal = Text.builder()
                .size(FlexFontSize.SM)
                .text("Nominal")
                .color("#555555")
                .flex(4)
                .build();
        Text dueDate = Text.builder()
                .size(FlexFontSize.SM)
                .text("Due Date")
                .color("#555555")
                .flex(4)
                .build();
        Filler filler = new Filler();

        Text idDebtValue = Text.builder()
                .size(FlexFontSize.SM)
                .text(debt.getDebtId().toString())
                .color("#111111")
                .flex(9)
                .build();
        Text debtorValue = Text.builder()
                .size(FlexFontSize.SM)
                .text(debt.getDebtor().getDisplayName())
                .color("#111111")
                .flex(9)
                .build();
        Text nominalValue = Text.builder()
                .size(FlexFontSize.SM)
                .text(formatter.format((double)debt.getDebtAmount()))
                .color("#111111")
                .flex(9)
                .build();
        Text dueDateValue = Text.builder()
                .size(FlexFontSize.SM)
                .text(debt.getDueDate().toString())
                .color("#111111")
                .flex(9)
                .build();

        PostbackAction settleDebt = new PostbackAction("Reject", "rejectDebt " + debt.getDebtId());
        Button rejectButton = Button.builder()
                .style(Button.ButtonStyle.SECONDARY)
                .height(Button.ButtonHeight.SMALL)
                .action(settleDebt)
                .flex(6)
                .build();

        PostbackAction remindDebt = new PostbackAction("Accept", "acceptDebt " + debt.getDebtId());
        Button acceptButton = Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .height(Button.ButtonHeight.SMALL)
                .action(remindDebt)
                .flex(6)
                .build();

        Box idBox = Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(asList(idDebt, idDebtValue))
                .build();
        Box debtorBox = Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(asList(debtor, debtorValue))
                .build();
        Box nominalBox = Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(asList(nominal, nominalValue))
                .build();
        Box dueDateBox = Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(asList(dueDate, dueDateValue))
                .build();
        Box buttonsBox = Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .margin(FlexMarginSize.MD)
                .contents(asList(filler, rejectButton, filler, acceptButton, filler))
                .build();

        Box container = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .margin(FlexMarginSize.MD)
                .spacing(FlexMarginSize.SM)
                .contents(asList(idBox, debtorBox, nominalBox, dueDateBox, buttonsBox))
                .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(asList(debtTitle, separator, container))
                .build();
    }
}