package com.ap_kelompok_1.debtreminder.handler.flex_message;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import java.util.function.Supplier;

import static java.util.Arrays.asList;

public class ProfileFlexMessage implements Supplier<FlexMessage> {

    private String urlPicture;
    private String displayName;
    private String userName;
    private String statusMessage;

    public ProfileFlexMessage(String urlPicture, String displayName, String userName,
        String statusMessage) {
        this.urlPicture = urlPicture;
        this.displayName = displayName;
        this.userName = userName;
        if (statusMessage == null) {
            this.statusMessage = "You have not set a status message.";
        } else {
            this.statusMessage = statusMessage;
        }
    }

    @Override
    public FlexMessage get() {
        final Box bodyBlock = BodyBlock();
        final Bubble bubble = Bubble.builder()
            .body(bodyBlock)
            .build();
        return new FlexMessage("Profile", bubble);
    }

    private Box BodyBlock() {
        final Text displayNameText = Text.builder()
            .align(FlexAlign.CENTER)
            .text(this.displayName)
            .size(FlexFontSize.XL)
            .weight(Text.TextWeight.BOLD)
            .build();
        final Text userNameText = Text.builder()
            .text("User Name")
            .color("#aaaaaa")
            .size(FlexFontSize.SM)
            .build();
        final Text userNameValue = Text.builder()
            .text(this.userName)
            .wrap(true)
            .color("#666666")
            .size(FlexFontSize.SM)
            .flex(2)
            .build();
        final Box displayUserName = Box.builder()
            .layout(FlexLayout.BASELINE)
            .spacing(FlexMarginSize.SM)
            .contents(asList(userNameText, userNameValue))
            .build();
        final Box contents = Box.builder()
            .layout(FlexLayout.VERTICAL)
            .margin(FlexMarginSize.LG)
            .spacing(FlexMarginSize.SM)
            .contents(asList(displayUserName))
            .build();
        return Box.builder()
            .layout(FlexLayout.VERTICAL)
            .contents(asList(displayNameText, contents))
            .build();
    }
}
