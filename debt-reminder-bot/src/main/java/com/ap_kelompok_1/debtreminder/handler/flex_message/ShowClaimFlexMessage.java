package com.ap_kelompok_1.debtreminder.handler.flex_message;

import com.ap_kelompok_1.debtreminder.model.Debt;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.*;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.text.NumberFormat;
import java.util.Locale;

import static java.util.Arrays.asList;

public class ShowClaimFlexMessage implements Supplier<FlexMessage> {

    final Separator separator = Separator.builder()
            .margin(FlexMarginSize.XL)
            .build();
    final Text debtTitle = Text.builder()
            .text("CLAIM")
            .weight(Text.TextWeight.BOLD)
            .size(FlexFontSize.XL)
            .margin(FlexMarginSize.MD)
            .build();
    private List<Debt> debtList;
    Locale localeID = new Locale("in", "ID");
    NumberFormat formatter = NumberFormat.getCurrencyInstance(localeID);


    public ShowClaimFlexMessage(List<Debt> debtList) {
        this.debtList = debtList;
    }

    @Override
    public FlexMessage get() {
        final Box bodyBlock = BodyBlock();
        final Bubble bubble = Bubble.builder()
                .body(bodyBlock)
                .build();
        return new FlexMessage("Your Claim", bubble);
    }

    private List<FlexComponent> initFlexComponentZeroList() {
        List<FlexComponent> FC = new ArrayList<>();
        FC.add(debtTitle);
        FC.add(separator);
        FC.add(separator);
        final Text noDebt = Text.builder()
                .text("You have no claims")
                .wrap(true)
                .weight(Text.TextWeight.BOLD)
                .size(FlexFontSize.Md)
                .margin(FlexMarginSize.MD)
                .build();
        final Text noDebtTotal = Text.builder()
                .text("TOTAL CLAIM = Rp 0,-")
                .wrap(true)
                .weight(Text.TextWeight.BOLD)
                .size(FlexFontSize.Md)
                .margin(FlexMarginSize.MD)
                .build();
        FC.add(noDebt);
        FC.add(noDebtTotal);
        return FC;
    }

    private List<FlexComponent> initFlexComponentNotZeroList(List<Debt> debtList) {
        List<FlexComponent> FC = new ArrayList<>();
        FC.add(debtTitle);
        FC.add(separator);
        FC.add(separator);
        final Text idDebt = Text.builder()
                .size(FlexFontSize.SM)
                .text("Debt Id")
                .color("#555555")
                .flex(4)
                .build();
        final Text debtor = Text.builder()
                .size(FlexFontSize.SM)
                .text("Debtor")
                .color("#555555")
                .flex(4)
                .build();
        final Text nominal = Text.builder()
                .size(FlexFontSize.SM)
                .text("Nominal")
                .color("#555555")
                .flex(4)
                .build();
        final Text dueDate = Text.builder()
                .size(FlexFontSize.SM)
                .text("Due Date")
                .color("#555555")
                .flex(4)
                .build();
        final Filler filler = new Filler();

        for (Debt debt : debtList) {

            Text idDebtValue = Text.builder()
                    .size(FlexFontSize.SM)
                    .text(debt.getDebtId().toString())
                    .color("#111111")
                    .flex(9)
                    .build();
            Text debtorValue = Text.builder()
                    .size(FlexFontSize.SM)
                    .text(debt.getDebtor().getDisplayName())
                    .color("#111111")
                    .flex(9)
                    .build();
            Text nominalValue = Text.builder()
                    .size(FlexFontSize.SM)
                    .text(formatter.format((double)debt.getDebtAmount()))
                    .color("#111111")
                    .flex(9)
                    .build();
            Text dueDateValue = Text.builder()
                    .size(FlexFontSize.SM)
                    .text(debt.getDueDate().toString())
                    .color("#111111")
                    .flex(9)
                    .build();

            PostbackAction settleDebt = new PostbackAction("Settle", "settleDebt " + debt.getDebtId());
            Button settleButton = Button.builder()
                    .style(Button.ButtonStyle.PRIMARY)
                    .height(Button.ButtonHeight.SMALL)
                    .action(settleDebt)
                    .flex(9)
                    .build();

            PostbackAction remindDebt = new PostbackAction("Remind", "remindDebt " + debt.getDebtId());
            Button remindButton = Button.builder()
                    .style(Button.ButtonStyle.PRIMARY)
                    .height(Button.ButtonHeight.SMALL)
                    .action(remindDebt)
                    .flex(9)
                    .build();

            Box idBox = Box.builder()
                    .layout(FlexLayout.HORIZONTAL)
                    .contents(asList(idDebt, idDebtValue))
                    .build();
            Box debtorBox = Box.builder()
                    .layout(FlexLayout.HORIZONTAL)
                    .contents(asList(debtor, debtorValue))
                    .build();
            Box nominalBox = Box.builder()
                    .layout(FlexLayout.HORIZONTAL)
                    .contents(asList(nominal, nominalValue))
                    .build();
            Box dueDateBox = Box.builder()
                    .layout(FlexLayout.HORIZONTAL)
                    .contents(asList(dueDate, dueDateValue))
                    .build();
            Box buttonsBox = Box.builder()
                    .layout(FlexLayout.HORIZONTAL)
                    .margin(FlexMarginSize.MD)
                    .contents(asList(settleButton, filler, remindButton))
                    .build();

            Box container = Box.builder()
                    .layout(FlexLayout.VERTICAL)
                    .margin(FlexMarginSize.MD)
                    .spacing(FlexMarginSize.SM)
                    .contents(asList(idBox, debtorBox, nominalBox, dueDateBox, buttonsBox))
                    .build();

            FC.add(container);
            FC.add(separator);
        }

        final Text totalDebt = Text.builder()
                .text("TOTAL")
                .size(FlexFontSize.Md)
                .weight(Text.TextWeight.BOLD)
                .color("#111111")
                .flex(4)
                .build();
        final Text totalDebtValue = Text.builder()
                .text(formatter.format(getDebtTotalNominal(debtList)))
                .size(FlexFontSize.Md)
                .weight(Text.TextWeight.BOLD)
                .color("#111111")
                .flex(9)
                .build();

        final PostbackAction settleAllDebt = new PostbackAction("Settle All", "settleAllDebt");
        final Button settleAllButton = Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .height(Button.ButtonHeight.SMALL)
                .action(settleAllDebt)
                .flex(9)
                .build();

        final PostbackAction remindAllDebt = new PostbackAction("Remind All", "remindAllDebt");
        final Button remindAllButton = Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .height(Button.ButtonHeight.SMALL)
                .action(remindAllDebt)
                .flex(9)
                .build();

        final Box textBox = Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .margin(FlexMarginSize.MD)
                .contents(asList(totalDebt, totalDebtValue))
                .build();


        final Box buttonsBox = Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .margin(FlexMarginSize.MD)
                .contents(asList(settleAllButton, filler, remindAllButton))
                .build();

        final Box totalBox = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .margin(FlexMarginSize.MD)
                .contents(asList(textBox, buttonsBox))
                .build();

        FC.add(totalBox);
        return FC;
    }

    private Box BodyBlock() {
        if (this.debtList.size() == 0) {
            return Box.builder()
                    .layout(FlexLayout.VERTICAL)
                    .contents(initFlexComponentZeroList())
                    .build();
        } else {
            return Box.builder()
                    .layout(FlexLayout.VERTICAL)
                    .contents(initFlexComponentNotZeroList(this.debtList))
                    .build();
        }
    }

    private long getDebtTotalNominal(List<Debt> debtList) {
        long totalNominal = 0;
        for (Debt debt : debtList) {
            totalNominal += debt.getDebtAmount();
        }
        return totalNominal;
    }
}