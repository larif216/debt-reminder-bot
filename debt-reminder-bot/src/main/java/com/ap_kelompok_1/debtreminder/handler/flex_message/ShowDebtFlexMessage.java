package com.ap_kelompok_1.debtreminder.handler.flex_message;

import com.ap_kelompok_1.debtreminder.model.Debt;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Filler;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.text.NumberFormat;
import java.util.Locale;

import static java.util.Arrays.asList;

public class ShowDebtFlexMessage implements Supplier<FlexMessage> {

    final Separator separator = Separator.builder()
        .margin(FlexMarginSize.XL)
        .build();
    final Text debtTitle = Text.builder()
        .text("DEBT")
        .weight(Text.TextWeight.BOLD)
        .size(FlexFontSize.XL)
        .margin(FlexMarginSize.MD)
        .build();
    private List<Debt> debtList;
    private Locale localeID = new Locale("in", "ID");
    private NumberFormat formatter = NumberFormat.getCurrencyInstance(localeID);

    public ShowDebtFlexMessage(List<Debt> debtList) {
        this.debtList = debtList;
    }

    @Override
    public FlexMessage get() {
        final Box bodyBlock = BodyBlock();
        final Bubble bubble = Bubble.builder()
            .body(bodyBlock)
            .build();
        return new FlexMessage("Your Debt", bubble);
    }

    private List<FlexComponent> initFlexComponentZeroList() {
        List<FlexComponent> FC = new ArrayList<>();
        FC.add(debtTitle);
        FC.add(separator);
        FC.add(separator);
        final Text noDebt = Text.builder()
            .text("You haven't debt to anyone or you have paid off all debts")
            .weight(Text.TextWeight.BOLD)
            .size(FlexFontSize.Md)
            .margin(FlexMarginSize.MD)
            .wrap(true)
            .build();
        final Text noDebtTotal = Text.builder()
            .text("TOTAL DEBT = Rp 0,-")
            .weight(Text.TextWeight.BOLD)
            .size(FlexFontSize.Md)
            .margin(FlexMarginSize.MD)
            .wrap(true)
            .build();
        FC.add(noDebt);
        FC.add(noDebtTotal);
        return FC;
    }

    private List<FlexComponent> initFlexComponentNotZeroList(List<Debt> debtList) {
        List<FlexComponent> FC = new ArrayList<>();
        FC.add(debtTitle);
        FC.add(separator);
        FC.add(separator);
        final Text idDebt = Text.builder()
            .size(FlexFontSize.SM)
            .text("Debt Id")
            .color("#555555")
            .flex(4)
            .build();
        final Text debtor = Text.builder()
            .size(FlexFontSize.SM)
            .text("Debtee")
            .color("#555555")
            .flex(4)
            .build();
        final Text nominal = Text.builder()
            .size(FlexFontSize.SM)
            .text("Nominal")
            .color("#555555")
            .flex(4)
            .build();
        final Text dueDate = Text.builder()
            .size(FlexFontSize.SM)
            .text("Due Date")
            .color("#555555")
            .flex(4)
            .build();

        for (Debt debt : debtList) {

            Text idDebtValue = Text.builder()
                .size(FlexFontSize.SM)
                .text(debt.getDebtId().toString())
                .color("#111111")
                .flex(9)
                .build();
            Text debtorValue = Text.builder()
                .size(FlexFontSize.SM)
                .text(debt.getDebtee().getDisplayName())
                .color("#111111")
                .flex(9)
                .build();
            Text nominalValue = Text.builder()
                .size(FlexFontSize.SM)
                .text(formatter.format((double)debt.getDebtAmount()))
                .color("#111111")
                .flex(9)
                .build();
            Text dueDateValue = Text.builder()
                .size(FlexFontSize.SM)
                .text(debt.getDueDate().toString())
                .color("#111111")
                .flex(9)
                .build();

            Box idBox = Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(asList(idDebt, idDebtValue))
                .build();
            Box debtorBox = Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(asList(debtor, debtorValue))
                .build();
            Box nominalBox = Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(asList(nominal, nominalValue))
                .build();
            Box dueDateBox = Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(asList(dueDate, dueDateValue))
                .build();

            Box container = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .margin(FlexMarginSize.MD)
                .spacing(FlexMarginSize.SM)
                .contents(asList(idBox, debtorBox, nominalBox, dueDateBox))
                .build();

            FC.add(container);
            FC.add(separator);
        }

        final Text totalDebt = Text.builder()
            .text("TOTAL")
            .size(FlexFontSize.Md)
            .weight(Text.TextWeight.BOLD)
            .color("#111111")
            .flex(4)
            .build();
        final Text totalDebtValue = Text.builder()
            .text(formatter.format(getDebtTotalNominal(debtList)))
            .size(FlexFontSize.Md)
            .weight(Text.TextWeight.BOLD)
            .color("#111111")
            .flex(9)
            .build();

        final Box totalBox = Box.builder()
            .layout(FlexLayout.HORIZONTAL)
            .margin(FlexMarginSize.MD)
            .contents(asList(totalDebt, totalDebtValue))
            .build();

        FC.add(totalBox);
        return FC;
    }

    private Box BodyBlock() {
        if (this.debtList.size() == 0) {
            return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(initFlexComponentZeroList())
                .build();
        } else {
            return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(initFlexComponentNotZeroList(this.debtList))
                .build();
        }
    }

    private long getDebtTotalNominal(List<Debt> debtList) {
        long totalNominal = 0;
        for (Debt debt : debtList) {
            totalNominal += debt.getDebtAmount();
        }
        return totalNominal;
    }
}