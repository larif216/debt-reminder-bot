package com.ap_kelompok_1.debtreminder.handler.message;

import com.ap_kelompok_1.debtreminder.model.Debt;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import java.text.NumberFormat;
import java.util.Locale;

public class Messages {
    private static Locale localeID = new Locale("in", "ID");
    private static NumberFormat formatter = NumberFormat.getCurrencyInstance(localeID);

    public static final Message REGISTER_NOT = new TextMessage(
            "You are not registered. Please register first by typing '/register [username]'");
    public static final Message REGISTER_USERNAME_SPEC =
            new TextMessage("Username must have length 5-10, and " +
                    "contains numerical and lowercase character and underscore only");
    public static final Message REGISTER_ALREADY = new TextMessage("You are already registered");
    public static final Message REGISTER_SUCCESS = new TextMessage("Registration success!");
    public static final Message REGISTER_TAKEN =
            new TextMessage("Username already taken. Try another one!");
    public static final Message REGISTER_NOT_PROVIDE_USERNAME =
            new TextMessage("You must provide username");
    public static final Message HELP_TEXT = new TextMessage(
            "Available of command \n" +
                    "/help - Show all valid command \n" +
                    "/profile - Show your display name and registered user name \n" +
                    "/bye - Let me leave this chat :( \n" +
                    "/register - Register your Line Account\n" +
                    "/debt <username> - To add your debt to someone\n" +
                    "/showDebt - Show your current debt\n" +
                    "/showclaim - Show your current claim(s)\n" +
                    "/cek-kurs - To show the current USD rate in IDR"
    );
    public static final Message OTHERS_TEXT = new TextMessage(
            "Invalid command! Please type '/help' to show all valid command"
    );

    public static final Message CANCEL_NOT = new TextMessage(
            "Invalid command! You are not in the middle of doing something"
    );

    public static final Message CANCEL_SUCCESS = new TextMessage(
            "Adding debt cancelled"
    );

    public static final Message BYE_GROUP = new TextMessage(
            "Leaving group"
    );
    public static final Message BYE_ROOM = new TextMessage(
            "Leaving room"
    );
    public static final Message BYE_CHAT = new TextMessage(
            "Bot can't leave from 1:1 chat"
    );

    public static final Message DEBT_ADD_DEBTEE_SUCCESS = new TextMessage(
            "Please enter the valid amount of debt in rupiah,\ne.g: 25000\nor type /cancel"
    );

    public static final Message DEBT_ADD_DEBTEE_FAIL = new TextMessage(
            "Please make sure to enter the right username"
    );

    public static final Message DEBT_ADD_DEBTEE_SELF = new TextMessage(
            "Can't debt to yourself :)"
    );

    public static final Message DEBT_ADD_DEBT_AMOUNT_SUCCESS = new TextMessage(
            "Please enter the due date in format: YYYY-MM-DD\n" +
                    "due date must not be earlier than today\n" +
                    "\ne.g: 2030-02-14\nor type /cancel"
    );

    public static final Message DEBT_ADD_DEBT_DUE_DATE_SUCCESS = new TextMessage(
            "Please wait for debtee to accept / reject debt before adding another one."
    );

    public static final Message NOT_IMPLEMENTED = new TextMessage(
            "Not implemented yet"
    );
    public static final Message SHOWDEBT_NODEBT = new TextMessage(
            "You haven't debt to anyone or you have paid off all debts\n\nTOTAL DEBT = Rp 0,-");
    public static final Message SHOWCLAIM_NOCLAIM = new TextMessage(
            "You have no claim \n\nTOTAL CLAIM = Rp 0,-");

    public static final Message REMIND_DEBT_FAIL = new TextMessage(
            "Debt is not exists."
    );
    public static final Message SETTLE_DEBT_FAIL = new TextMessage(
            "Debt is not exists."
    );
    public static final Message REMIND_DEBT_CONFIRM_FAIL = new TextMessage(
            "Please type /yes or /cancel"
    );
    public static final Message REMIND_DEBT_CONFIRM_CANCEL = new TextMessage(
            "Remind debtor cancelled."
    );
    public static final Message REMIND_DEBT_ERROR = new TextMessage(
            "Something wrong with our database (Remind Debt Confirmation)"
    );
    public static final Message REJECTED_DEBT = new TextMessage("your debt request was rejected!");
    public static final Message CONFIRMED_DEBT =
            new TextMessage("yay, your debt request was accepted!");
    public static final Message SETTLE_DEBT_CONFIRM_FAIL = new TextMessage(
            "Please type /yes or /cancel"
    );
    public static final Message SETTLE_DEBT_CANCEL = new TextMessage(
            "Settle debt cancelled."
    );
    public static final Message REMIND_ALL_DEBT_SUCCESS = new TextMessage(
            "Successfully reminded to all debtors"
    );
    public static final Message REMIND_ALL_DEBT_CONFIRM_CANCEL = new TextMessage(
            "Remind all debtors cancelled."
    );
    public static final Message SETTLE_ALL_DEBT_SUCCESS = new TextMessage(
            "Successfully settled all debts."
    );
    public static final Message SETTLE_ALL_DEBT_CANCEL = new TextMessage(
            "Settle all debt cancelled."
    );
    public static final Message WAITING_CONFIRMATION_CANT_ADD_NEW_DEBT = new TextMessage(
            "Can't add new Debt, please let the debtee accept first."
    );
    public static final Message CONFIRM_REMIND_ALL_DEBT = new TextMessage(
            "Are you sure want to remind all debtors?\n" +
                    "Type /yes or /cancel"
    );
    public static final Message CONFIRM_SETTLE_ALL_DEBT = new TextMessage(
            "Are you sure want to settle all debts?\n" +
                    "Type /yes or /cancel"
    );

    public static final Message verifyYourDebt(Debt debt) {
        return new TextMessage(
            "Please accept this debt: " + "\n" +
                "User : " + debt.getDebtor().getUserName() + "\n" +
                "Amount : " + formatter.format((double)debt.getDebtAmount()) + "\n" +
                "Due Date : " + debt.getDueDate() + "\n" +
                "/reject or /accept"
        );
    }


    public static final Message remindDebtSuccess(Debt debt) {
        return new TextMessage(
                "Successfully reminded " + debt.getDebtor().getUserName() +
                        ". Hopefully it's gonna be paid soon."
        );
    }

    public static final Message confirmRemindDebt(Debt debt) {
        return new TextMessage(
            "Are you sure want to remind debt with details:" + "\n" +
                "Debtor's name: " + debt.getDebtor().getDisplayName() + "\n" +
                "Debtor's username: " + debt.getDebtor().getUserName() + "\n" +
                "Amount: " + formatter.format((double)debt.getDebtAmount()) + "\n" +
                "Due Date: " + debt.getDueDate() + "\n" +
                "Type /yes or /cancel"
        );
    }

    public static final Message remindDebt(Debt debt) {
        return new TextMessage(
            "You've been reminded by " + debt.getDebtee().getUserName() +
                " with details:\n" +
                "Debtee's name: " + debt.getDebtee().getDisplayName() + "\n" +
                "Amount: " + formatter.format((double)debt.getDebtAmount()) + "\n" +
                "Due Date: " + debt.getDueDate() + "\n" +
                "Please pay soon before the due date."
        );
    }

    public static final Message settleDebt(Debt debt) {
        return new TextMessage(
            "Your debt to " + debt.getDebtee().getUserName() +
                " have been settled with details:\n" +
                "Debtee's name: " + debt.getDebtee().getDisplayName() + "\n" +
                "Amount: " + formatter.format((double)debt.getDebtAmount())
        );
    }

    public static final Message settleDebtSuccess(Debt debt) {
        return new TextMessage(
            "Successfully settled debt from " + debt.getDebtor().getUserName() +
                " with details:\n" +
                "Debtor's name: " + debt.getDebtor().getDisplayName() + "\n" +
                "Amount: " + formatter.format((double)debt.getDebtAmount())
        );
    }

    public static final Message cancelDebtSuccess(String name) {
        return new TextMessage(
                name + " decided to cancel his / her debt request."
        );
    }

    public static final Message confirmSettleDebt(Debt debt) {
        return new TextMessage(
            "Are you sure want to settle debt with details:" + "\n" +
                "Debtor's name: " + debt.getDebtor().getDisplayName() + "\n" +
                "Debtor's username: " + debt.getDebtor().getUserName() + "\n" +
                "Amount: " + formatter.format((double)debt.getDebtAmount()) + "\n" +
                "Due Date: " + debt.getDueDate() + "\n" +
                "Type /yes or /cancel"
        );
    }

    public static final Message ACCEPT_DEBT = new TextMessage(
            "Accepted debt."
    );

    public static final Message REJECT_DEBT = new TextMessage(
            "Rejected debt."
    );
}
