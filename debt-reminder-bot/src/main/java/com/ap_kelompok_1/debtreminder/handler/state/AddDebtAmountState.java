package com.ap_kelompok_1.debtreminder.handler.state;

import com.ap_kelompok_1.debtreminder.controller.chat_source.ChatSource;
import com.ap_kelompok_1.debtreminder.handler.message.Messages;
import com.ap_kelompok_1.debtreminder.model.Debt;
import com.ap_kelompok_1.debtreminder.model.LineUser;
import com.linecorp.bot.model.message.Message;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class AddDebtAmountState extends State {
    public static final String DB_COL_NAME = "ADD_DEBT_AMOUNT";
    private static Pattern amountPattern = Pattern.compile("^[1-9][0-9]*$");

    public AddDebtAmountState() {
        this.responses = new ArrayList<>();
    }

    public void clearResponses() {
        this.responses.clear();
    }

    private boolean isAmountValid(String amount) {
        /**
         * amount is valid if it matches the pattern
         * @version 1.0
         * @author tester_jdocs
         */
        Matcher matcher = amountPattern.matcher(amount);
        return matcher.matches();
    }

    public List<Message> register(String userName, String userId, String displayName) {
        responses.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        return responses;
    }

    public List<Message> profile(String urlPicture, String displayName, String userName,
        String statusMessage, String userId) {
        responses.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        return responses;
    }

    public List<Message> showDebt(String userId) {
        responses.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        return responses;
    }

    public List<Message> showClaim(String userId) {
        responses.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        return responses;
    }

    public List<Message> bye(ChatSource chatSource, String userId) {
        responses.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        return responses;
    }

    public List<Message> help(String userId) {
        responses.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        return responses;
    }

    public List<Message> addDebtee(String userId, String debteeId) {
        responses.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        return responses;
    }

    public List<Message> addAmount(Long amount) {
        responses.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        return responses;
    }

    public List<Message> addDueDate(Date dueDate) {
        responses.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        return responses;
    }

    public List<Message> others(String amount, String userId) {
        if (isAmountValid(amount)) {
            Debt debt = debtRepository.findUnverifiedDebtByDebtorUserid(userId);
            LineUser debtor = debt.getDebtor();

            debtor.setState(AddDueDateState.DB_COL_NAME);
            debt.setDebtAmount(Long.parseLong(amount));

            lineUserRepository.save(debtor);
            debtRepository.save(debt);
            responses.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        } else {
            responses.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        }
        return responses;
    }

    public List<Message> cancel(String userId) {
        responses.add(Messages.CANCEL_SUCCESS);
        LineUser lineUser = lineUserRepository.findLineUserByUserId(userId);
        Debt debt = debtRepository.findUnverifiedDebtByDebtorUserid(userId);
        lineUser.setState(RegisteredState.DB_COL_NAME);
        lineUserRepository.save(lineUser);
        debtRepository.deleteDebtByDebtId(debt.getDebtId());
        return responses;
    }

    public List<Message> remindDebtor(Long debtId) {
        responses.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        return responses;
    }

    public List<Message> remindAllDebtor(String userId) {
        responses.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        return responses;
    }

    public List<Message> settleDebt(Long debtId) {
        responses.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        return responses;
    }

    public List<Message> settleAllDebt(String userId) {
        responses.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        return responses;
    }

    public List<Message> kurs(String userId) {
        responses.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        return responses;
    }

}