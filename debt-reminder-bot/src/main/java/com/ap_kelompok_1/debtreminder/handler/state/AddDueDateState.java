package com.ap_kelompok_1.debtreminder.handler.state;

import com.ap_kelompok_1.debtreminder.controller.chat_source.ChatSource;
import com.ap_kelompok_1.debtreminder.handler.flex_message.AcceptRejectFlexMessage;
import com.ap_kelompok_1.debtreminder.handler.message.Messages;
import com.ap_kelompok_1.debtreminder.model.Debt;
import com.ap_kelompok_1.debtreminder.model.LineUser;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.Message;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class AddDueDateState extends State {
    public static final String DB_COL_NAME = "ADD_DUE_DATE";

    private static Pattern gregorianDatePattern = Pattern.compile(
        "^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$"
            + "|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$"
            + "|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$"
            + "|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$");

    private static SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

    public AddDueDateState() {
        this.responses = new ArrayList<>();
    }

    public void clearResponses() {
        this.responses.clear();
    }

    private boolean isDateValid(String date) throws ParseException {
        /**
         * date is valid if it matches the pattern and not earlier than today 
         * @version 1.0
         * @author ipman
         */
        Matcher matcher = gregorianDatePattern.matcher(date);
        if (matcher.matches()) {
            Date date1 = new Date();
            Date date2 = dateFormatter.parse(date);
            return (date1.compareTo(date2) <= 0);
        } else {
            return false;
        }
    }

    public List<Message> register(String userName, String userId, String displayName) {
        responses.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        return responses;
    }

    public List<Message> profile(String urlPicture, String displayName, String userName,
        String statusMessage, String userId) {
        responses.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        return responses;
    }

    public List<Message> showDebt(String userId) {
        responses.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        return responses;
    }

    public List<Message> showClaim(String userId) {
        responses.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        return responses;
    }

    public List<Message> bye(ChatSource chatSource, String userId) {
        responses.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        return responses;
    }

    public List<Message> help(String userId) {
        responses.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        return responses;
    }

    public List<Message> addDebtee(String userId, String debteeId) {
        responses.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        return responses;
    }

    public List<Message> addAmount(Long amount) {
        responses.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        return responses;
    }

    public List<Message> addDueDate(Date dueDate) {
        responses.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        return responses;
    }

    public List<Message> others(String date, String userId) throws ParseException {
        if (isDateValid(date)) {
            Debt debt = debtRepository.findUnverifiedDebtByDebtorUserid(userId);
            LineUser debtor = debt.getDebtor();
            LineUser debtee = debt.getDebtee();
            Date formattedDate = dateFormatter.parse(date);

            debt.setDueDate(formattedDate);
            debtor.setState(RegisteredState.DB_COL_NAME);

            lineUserRepository.save(debtor);
            debtRepository.save(debt);

            AcceptRejectFlexMessage acceptRejectFlexMessage =
                    new AcceptRejectFlexMessage(debt);

            lineMessagingClient.pushMessage(
                new PushMessage(debtee.getUserId(),acceptRejectFlexMessage.get()));
            responses.add(Messages.DEBT_ADD_DEBT_DUE_DATE_SUCCESS);
        } else {
            responses.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        }
        return responses;
    }

    public List<Message> cancel(String userId) {
        responses.add(Messages.CANCEL_SUCCESS);
        LineUser lineUser = lineUserRepository.findLineUserByUserId(userId);
        Debt debt = debtRepository.findUnverifiedDebtByDebtorUserid(userId);
        lineUser.setState(RegisteredState.DB_COL_NAME);
        lineUserRepository.save(lineUser);
        debtRepository.deleteDebtByDebtId(debt.getDebtId());
        return responses;
    }

    public List<Message> remindDebtor(Long debtId) {
        responses.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        return responses;
    }

    public List<Message> remindAllDebtor(String userId) {
        responses.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        return responses;
    }

    public List<Message> settleDebt(Long debtId) {
        responses.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        return responses;
    }

    public List<Message> settleAllDebt(String userId) {
        responses.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        return responses;
    }

    public List<Message> kurs(String userId) {
        responses.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        return responses;
    }

}
