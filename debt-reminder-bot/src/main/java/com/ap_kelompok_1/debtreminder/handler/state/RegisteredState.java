package com.ap_kelompok_1.debtreminder.handler.state;

import com.ap_kelompok_1.debtreminder.controller.chat_source.ChatSource;
import com.ap_kelompok_1.debtreminder.handler.flex_message.ProfileFlexMessage;
import com.ap_kelompok_1.debtreminder.handler.flex_message.ShowClaimFlexMessage;
import com.ap_kelompok_1.debtreminder.handler.flex_message.ShowDebtFlexMessage;
import com.ap_kelompok_1.debtreminder.handler.message.Messages;
import com.ap_kelompok_1.debtreminder.handler.state.helper.KursHelper;
import com.ap_kelompok_1.debtreminder.model.Debt;
import com.ap_kelompok_1.debtreminder.model.LineUser;
import com.linecorp.bot.model.message.Message;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Component
public class RegisteredState extends State {
    public static final String DB_COL_NAME = "REGISTERED";
    private KursHelper kursHelper = new KursHelper();

    public RegisteredState() {
        this.responses = new ArrayList<>();
    }

    public void clearResponses() {
        this.responses.clear();
    }

    public List<Message> register(String userName, String userId, String displayName) {
        responses.add(Messages.REGISTER_ALREADY);
        return responses;
    }

    public List<Message> profile(String urlPicture, String displayName, String userName,
        String statusMessage, String userId) {
        ProfileFlexMessage profileFlexMessage = new ProfileFlexMessage(
            urlPicture,
            displayName,
            userName,
            statusMessage
        );
        responses.add(profileFlexMessage.get());
        return responses;
    }

    public List<Message> showDebt(String userId) {
        List<Debt> debtList = debtRepository.findDebtsByDebtorUserId(userId);
        ShowDebtFlexMessage showDebtFlexMessage = new ShowDebtFlexMessage(VerifiedDebt(debtList));
        responses.add(showDebtFlexMessage.get());
        return responses;
    }

    public List<Message> showClaim(String userId) {
        List<Debt> debtList = debtRepository.findDebtsByDebteeUserId(userId);
        ShowClaimFlexMessage showClaimFlexMessage = new ShowClaimFlexMessage(VerifiedDebt(debtList));
        responses.add(showClaimFlexMessage.get());
        return responses;
    }

    public List<Message> bye(ChatSource chatSource, String userId)
        throws InterruptedException, ExecutionException {
        switch (chatSource.getType()) {
            case ChatSource.GROUP:
                this.lineMessagingClient.leaveGroup(
                    chatSource.getId()
                ).get();
                responses.add(Messages.BYE_GROUP);
                break;
            case ChatSource.ROOM:
                this.lineMessagingClient.leaveGroup(
                    chatSource.getId()
                ).get();
                responses.add(Messages.BYE_ROOM);
                break;
            case ChatSource.CHAT:
                responses.add(Messages.BYE_CHAT);
                break;
        }
        return responses;
    }

    public List<Message> help(String userId) {
        responses.add(Messages.HELP_TEXT);
        return responses;
    }

    public List<Message> addDebtee(String userId, String debteeId) {
        // TODO : handle send debt to self (disabled for testing purposes.)
        // if(userId == debteeId) {
        //    responses.add(Messages.DEBT_ADD_DEBTEE_SELF);
        //} else
        // TODO : kalau lagi ditengah state nambah hutang, terus ada masuk konfirmasi hutang usernya gimana?
        // solusi alt : command /accept debtId dibolehin.
        // TODO : kalau banyak konfirmasi hutang gimana ?
        // solusi alt : kirim flexmessage yang kalau dipencet /accept debtId ?
        // TODO : boleh kirim banyak hutang sebelum diconfirm ?
        // solusi alt : bikin kolom terpisah selain verified.

        if (debteeId == null) {
            responses.add(Messages.DEBT_ADD_DEBTEE_FAIL);
        } else {
            LineUser debtor = lineUserRepository.findLineUserByUserId(userId);
            LineUser debtee = lineUserRepository.findLineUserByUserId(debteeId);
            Debt debt = new Debt();

            debtor.setState(AddDebtAmountState.DB_COL_NAME);
            debt.setDebtor(debtor);
            debt.setDebtee(debtee);
            debt.setVerified(false);

            lineUserRepository.save(debtor);
            debtRepository.save(debt);
            responses.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        }
        return responses;
    }

    public List<Message> addAmount(Long amount) {
        responses.add(Messages.OTHERS_TEXT);
        return responses;
    }

    public List<Message> addDueDate(Date dueDate) {
        responses.add(Messages.OTHERS_TEXT);
        return responses;
    }

    public List<Message> others(String command, String userId) {
        responses.add(Messages.OTHERS_TEXT);
        return responses;
    }

    public List<Message> cancel(String userId) {
        responses.add(Messages.CANCEL_NOT);
        return responses;
    }

    public List<Message> remindDebtor(Long debtId) {
        Debt debt = debtRepository.findDebtByDebtId(debtId);
        if (debt == null) {
            responses.add(Messages.REMIND_DEBT_FAIL);
        } else {
            LineUser debtee = debt.getDebtee();
            debt.setRemindable(true);
            debtee.setState(RemindDebtConfirmationState.DB_COL_NAME);
            lineUserRepository.save(debtee);
            debtRepository.save(debt);
            responses.add(Messages.confirmRemindDebt(debt));
        }
        return responses;
    }

    public List<Message> remindAllDebtor(String userId) {
        List<Debt> debts = debtRepository.findVerifiedDebtsByDebteeUserId(userId);
        LineUser debtee = lineUserRepository.findLineUserByUserId(userId);
        if (debts.size() == 0) {
            responses.add(Messages.REMIND_DEBT_FAIL);
        } else {
            debtee.setState(RemindAllDebtConfirmationState.DB_COL_NAME);
            lineUserRepository.save(debtee);
            responses.add(Messages.CONFIRM_REMIND_ALL_DEBT);
        }
        return responses;
    }

    public List<Message> settleDebt(Long debtId) {
        Debt debt = debtRepository.findDebtByDebtId(debtId);
        if (debt == null) {
            responses.add(Messages.SETTLE_DEBT_FAIL);
        } else {
            LineUser debtee = debt.getDebtee();
            debt.setSettleable(true);
            debtee.setState(SettleDebtConfirmationState.DB_COL_NAME);
            lineUserRepository.save(debtee);
            debtRepository.save(debt);
            responses.add(Messages.confirmSettleDebt(debt));
        }
        return responses;
    }

    public List<Message> settleAllDebt(String userId) {
        List<Debt> debts = debtRepository.findVerifiedDebtsByDebteeUserId(userId);
        LineUser debtee = lineUserRepository.findLineUserByUserId(userId);
        if (debts.size() == 0) {
            responses.add(Messages.SETTLE_DEBT_FAIL);
        } else {
            debtee.setState(SettleAllDebtConfirmationState.DB_COL_NAME);
            lineUserRepository.save(debtee);
            responses.add(Messages.CONFIRM_SETTLE_ALL_DEBT);
        }
        return responses;
    }

    private List<Debt> VerifiedDebt(List<Debt> debtList){
        List<Debt> verifiedDebtList = new ArrayList<>();

        for(Debt debt : debtList){
            if(debt.getVerified())
                verifiedDebtList.add(debt);
        }

        return verifiedDebtList;
    }

    public List<Message> kurs(String userId) {
        return kursHelper.getResponse();
    }


}
