package com.ap_kelompok_1.debtreminder.handler.state;

import com.ap_kelompok_1.debtreminder.controller.chat_source.ChatSource;
import com.ap_kelompok_1.debtreminder.handler.message.Messages;
import com.ap_kelompok_1.debtreminder.model.Debt;
import com.ap_kelompok_1.debtreminder.model.LineUser;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.Message;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class RemindDebtConfirmationState extends State {
    public static final String DB_COL_NAME = "REMIND_DEBT_CONFIRMATION";

    public RemindDebtConfirmationState() {
        this.responses = new ArrayList<>();
    }

    public void clearResponses() {
        this.responses.clear();
    }

    public List<Message> register(String userName, String userId, String displayName) {
        responses.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> profile(String urlPicture, String displayName, String userName,
        String statusMessage, String userId) {
        responses.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> showDebt(String userId) {
        responses.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> showClaim(String userId) {
        responses.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> bye(ChatSource chatSource, String userId) {
        responses.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> help(String userId) {
        responses.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> addDebtee(String userId, String debteeId) {
        responses.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> addAmount(Long amount) {
        responses.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> addDueDate(Date dueDate) {
        responses.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> others(String command, String userId) {
        List<Debt> debts = debtRepository.findRemindableDebtsByDebteeId(userId);
        if (command.equalsIgnoreCase("/yes")) {
            if (debts.size() == 1) {
                Debt debt = debts.get(0);
                LineUser debtee = debt.getDebtee();
                LineUser debtor = debt.getDebtor();
                lineMessagingClient.pushMessage(
                    new PushMessage(debtor.getUserId(), Messages.remindDebt(debt)));
                debt.setRemindable(false);
                debtee.setState(RegisteredState.DB_COL_NAME);
                lineUserRepository.save(debtee);
                debtRepository.save(debt);

                responses.add(Messages.remindDebtSuccess(debt));
            } else {
                responses.add(Messages.REMIND_DEBT_ERROR);
            }
        } else {
            responses.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        }
        return responses;
    }

    public List<Message> cancel(String userId) {
        List<Debt> debts = debtRepository.findRemindableDebtsByDebteeId(userId);
        if (debts.size() == 1) {
            Debt debt = debts.get(0);
            debt.setRemindable(false);
            LineUser debtee = debt.getDebtee();
            debtee.setState(RegisteredState.DB_COL_NAME);
            lineUserRepository.save(debtee);
            debtRepository.save(debt);

            responses.add(Messages.REMIND_DEBT_CONFIRM_CANCEL);
        } else {
            responses.add(Messages.REMIND_DEBT_ERROR);
        }
        return responses;
    }

    public List<Message> remindDebtor(Long debtId) {
        responses.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> remindAllDebtor(String userId) {
        responses.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> settleDebt(Long debtId) {
        responses.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> settleAllDebt(String userId) {
        responses.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> kurs(String userId) {
        responses.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        return responses;
    }

}
