package com.ap_kelompok_1.debtreminder.handler.state;

import com.ap_kelompok_1.debtreminder.controller.chat_source.ChatSource;
import com.ap_kelompok_1.debtreminder.handler.message.Messages;
import com.ap_kelompok_1.debtreminder.model.Debt;
import com.ap_kelompok_1.debtreminder.model.LineUser;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.Message;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class SettleAllDebtConfirmationState extends State {
    public static final String DB_COL_NAME = "SETTLE_ALL_DEBT_CONFIRMATION";

    public SettleAllDebtConfirmationState() {
        this.responses = new ArrayList<>();
    }

    public void clearResponses() {
        this.responses.clear();
    }

    public List<Message> register(String userName, String userId, String displayName) {
        responses.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> profile(String urlPicture, String displayName, String userName,
                                 String statusMessage, String userId) {
        responses.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> showDebt(String userId) {
        responses.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> showClaim(String userId) {
        responses.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> bye(ChatSource chatSource, String userId) {
        responses.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> help(String userId) {
        responses.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> addDebtee(String userId, String debteeId) {
        responses.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> addAmount(Long amount) {
        responses.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> addDueDate(Date dueDate) {
        responses.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> others(String command, String userId) {
        List<Debt> debts = debtRepository.findVerifiedDebtsByDebteeUserId(userId);
        LineUser debtee = lineUserRepository.findLineUserByUserId(userId);
        if (command.equalsIgnoreCase("/yes")) {
            debtee.setState(RegisteredState.DB_COL_NAME);
            lineUserRepository.save(debtee);
            for (Debt debt : debts) {
                LineUser debtor = debt.getDebtor();
                lineMessagingClient.pushMessage(
                        new PushMessage(debtor.getUserId(), Messages.settleDebt(debt)));
            }
            debtRepository.deleteVerifiedDebtsByDebteeUserId(debtee.getUserId());
            responses.add(Messages.SETTLE_ALL_DEBT_SUCCESS);
        } else {
            responses.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        }
        return responses;
    }

    public List<Message> cancel(String userId) {
        LineUser debtee = lineUserRepository.findLineUserByUserId(userId);
        debtee.setState(RegisteredState.DB_COL_NAME);
        lineUserRepository.save(debtee);
        responses.add(Messages.SETTLE_ALL_DEBT_CANCEL);
        return responses;
    }

    public List<Message> remindDebtor(Long debtId) {
        responses.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> remindAllDebtor(String userId) {
        responses.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> settleDebt(Long debtId) {
        responses.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> settleAllDebt(String userId) {
        responses.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        return responses;
    }

    public List<Message> kurs(String userId) {
        responses.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        return responses;
    }

}
