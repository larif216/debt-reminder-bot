package com.ap_kelompok_1.debtreminder.handler.state;

import com.ap_kelompok_1.debtreminder.controller.chat_source.ChatSource;
import com.ap_kelompok_1.debtreminder.handler.message.Messages;
import com.ap_kelompok_1.debtreminder.model.Debt;
import com.ap_kelompok_1.debtreminder.model.LineUser;
import com.ap_kelompok_1.debtreminder.repository.DebtRepository;
import com.ap_kelompok_1.debtreminder.repository.LineUserRepository;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.Message;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

public abstract class State {
    @Autowired
    LineMessagingClient lineMessagingClient;
    @Autowired
    LineUserRepository lineUserRepository;
    @Autowired
    DebtRepository debtRepository;
    protected List<Message> responses;

    public abstract List<Message> register(String userName, String userId, String displayName);

    public abstract List<Message> profile(String urlPicture, String displayName, String userName,
                                          String statusMessage, String userId);

    public abstract List<Message> showDebt(String userId);

    public abstract List<Message> showClaim(String userId);

    public abstract List<Message> bye(ChatSource chatSource, String userId)
        throws InterruptedException, ExecutionException;

    public abstract List<Message> help(String userId);

    public abstract List<Message> addDebtee(String userId, String debteeId);

    public abstract List<Message> addAmount(Long amount);

    public abstract List<Message> addDueDate(Date dueDate);

    public abstract List<Message> others(String command, String userId) throws ParseException;

    public abstract List<Message> cancel(String userId);

    public abstract List<Message> remindDebtor(Long debtId);

    public abstract List<Message> remindAllDebtor(String userId);

    public abstract List<Message> settleDebt(Long debtId);

    public abstract List<Message> settleAllDebt(String userId);

    public List<Message> reject(Long debtId) {
        Debt debt = debtRepository.findUnverifiedDebtByDebtId(debtId);
        LineUser debtor = debt.getDebtor();
        lineMessagingClient.pushMessage(
                new PushMessage(debtor.getUserId(), Messages.REJECTED_DEBT));
        debtRepository.deleteDebtByDebtId(debt.getDebtId());
        responses.add(Messages.REJECT_DEBT);
        return responses;
    }

    public List<Message> accept(Long debtId) {
        Debt debt = debtRepository.findUnverifiedDebtByDebtId(debtId);
        debt.setVerified(true);
        LineUser debtor = debt.getDebtor();
        debtRepository.save(debt);
        lineMessagingClient.pushMessage(
                new PushMessage(debtor.getUserId(), Messages.CONFIRMED_DEBT));
        responses.add(Messages.ACCEPT_DEBT);
        return responses;
    }

    public abstract List<Message> kurs(String userId);

    public abstract void clearResponses();
}