package com.ap_kelompok_1.debtreminder.handler.state;

import com.ap_kelompok_1.debtreminder.controller.chat_source.ChatSource;
import com.linecorp.bot.model.message.Message;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class UnimplementedState extends State {
    public static final String DB_COL_NAME = "UNIMPLEMENTED";

    public UnimplementedState() {
        this.responses = new ArrayList<>();
    }

    public void clearResponses() {
        this.responses.clear();
    }

    public List<Message> register(String userName, String userId, String displayName) {
        return responses;
    }

    public List<Message> profile(String urlPicture, String displayName, String userName,
        String statusMessage, String userId) {
        return responses;
    }

    public List<Message> showDebt(String userId) {
        return responses;
    }


    public List<Message> showClaim(String userId) { return responses; }

    public List<Message> bye(ChatSource chatSource, String userId) {
        return responses;
    }

    public List<Message> help(String userId) {
        return responses;
    }

    public List<Message> addDebtee(String userId, String debteeId) {
        return responses;
    }

    public List<Message> addAmount(Long amount) {
        return responses;
    }

    public List<Message> addDueDate(Date dueDate) {
        return responses;
    }

    public List<Message> others(String command, String userId) {
        return responses;
    }

    public List<Message> cancel(String userId) {
        return responses;
    }

    public List<Message> remindDebtor(Long debtId) {
        return responses;
    }

    public List<Message> remindAllDebtor(String userId) {
        return responses;
    }

    public List<Message> settleDebt(Long debtId) {
        return responses;
    }

    public List<Message> settleAllDebt(String userId) {
        return responses;
    }

    public List<Message> reject(Long debtId) {
        return responses;
    }

    public List<Message> accept(Long debtId) {
        return responses;
    }

    public List<Message> kurs(String userId) {  return responses; }
}
