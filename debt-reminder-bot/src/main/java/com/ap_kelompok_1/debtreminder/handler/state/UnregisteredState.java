package com.ap_kelompok_1.debtreminder.handler.state;

import com.ap_kelompok_1.debtreminder.controller.chat_source.ChatSource;
import com.ap_kelompok_1.debtreminder.handler.message.Messages;
import com.ap_kelompok_1.debtreminder.model.LineUser;
import com.linecorp.bot.model.message.Message;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// TODO : behavior bye di state ini gimana ya?

@Component
public class UnregisteredState extends State {
    public static final String DB_COL_NAME = "UNREGISTERED";
    private static Pattern userNamePattern = Pattern.compile("^[a-z0-9_]{5,10}$");

    public UnregisteredState() {
        this.responses = new ArrayList<>();
    }

    public void clearResponses() {
        this.responses.clear();
    }

    private boolean isUserNameValid(String userName) {
        /**
         * username is valid if it matches the pattern
         * @version 1.0
         * @author ipman
         */
        Matcher matcher = userNamePattern.matcher(userName);
        return matcher.matches();
    }

    public List<Message> showUsername() {
        responses.add(Messages.REGISTER_NOT);
        responses.add(Messages.REGISTER_USERNAME_SPEC);
        return responses;
    }

    public List<Message> register(String userName, String userId, String displayName) {
        /**
         * if the user is successfully registered, the new user will have REGISTERED state
         * in the database, thus changing from UNREGISTERED to REGISTERED state
         * @version 1.0
         * @author ipman
         */
        if (userName.equals("") && userId.equals("") && displayName.equals("")) {
            responses.add(Messages.REGISTER_NOT_PROVIDE_USERNAME);
        } else if (this.isUserNameValid(userName)) {
            if (lineUserRepository.isUserNameUnique(userName)) {
                responses.add(Messages.REGISTER_TAKEN);
            } else {
                LineUser newUser = new LineUser(userId, displayName, userName);
                lineUserRepository.save(newUser);
                responses.add(Messages.REGISTER_SUCCESS);
            }
        } else {
            responses.add(Messages.REGISTER_USERNAME_SPEC);
        }
        return responses;
    }

    public List<Message> profile(String urlPicture, String displayName, String userName,
        String statusMessage, String userId) {
        responses.add(Messages.REGISTER_NOT);
        responses.add(Messages.REGISTER_USERNAME_SPEC);
        return responses;
    }

    public List<Message> showDebt(String userId) {
        responses.add(Messages.REGISTER_NOT);
        responses.add(Messages.REGISTER_USERNAME_SPEC);
        return responses;
    }

    public List<Message> showClaim(String userId) {
        responses.add(Messages.REGISTER_NOT);
        responses.add(Messages.REGISTER_USERNAME_SPEC);
        return responses;
    }

    public List<Message> showClaim() {
        responses.add(Messages.REGISTER_NOT);
        responses.add(Messages.REGISTER_USERNAME_SPEC);
        return responses;
    }

    public List<Message> bye(ChatSource chatSource, String userId) {
        responses.add(Messages.REGISTER_NOT);
        responses.add(Messages.REGISTER_USERNAME_SPEC);
        return responses;
    }

    public List<Message> help(String userId) {
        responses.add(Messages.REGISTER_NOT);
        responses.add(Messages.REGISTER_USERNAME_SPEC);
        return responses;
    }

    public List<Message> addDebtee(String userId, String debteeId) {
        responses.add(Messages.REGISTER_NOT);
        responses.add(Messages.REGISTER_USERNAME_SPEC);
        return responses;
    }

    public List<Message> addAmount(Long amount) {
        responses.add(Messages.REGISTER_NOT);
        responses.add(Messages.REGISTER_USERNAME_SPEC);
        return responses;
    }

    public List<Message> addDueDate(Date dueDate) {
        responses.add(Messages.REGISTER_NOT);
        responses.add(Messages.REGISTER_USERNAME_SPEC);
        return responses;
    }

    public List<Message> others(String command, String userId) {
        responses.add(Messages.REGISTER_NOT);
        responses.add(Messages.REGISTER_USERNAME_SPEC);
        return responses;
    }

    public List<Message> cancel(String userId) {
        responses.add(Messages.REGISTER_NOT);
        responses.add(Messages.REGISTER_USERNAME_SPEC);
        return responses;
    }

    public List<Message> remindDebtor(Long debtId) {
        responses.add(Messages.REGISTER_NOT);
        responses.add(Messages.REGISTER_USERNAME_SPEC);
        return responses;
    }

    public List<Message> remindAllDebtor(String userId) {
        responses.add(Messages.REGISTER_NOT);
        responses.add(Messages.REGISTER_USERNAME_SPEC);
        return responses;
    }

    public List<Message> settleDebt(Long debtId) {
        responses.add(Messages.REGISTER_NOT);
        responses.add(Messages.REGISTER_USERNAME_SPEC);
        return responses;
    }

    public List<Message> settleAllDebt(String userId) {
        responses.add(Messages.REGISTER_NOT);
        responses.add(Messages.REGISTER_USERNAME_SPEC);
        return responses;
    }

    public List<Message> reject(Long debtId) {
        responses.add(Messages.REGISTER_NOT);
        responses.add(Messages.REGISTER_USERNAME_SPEC);
        return responses;
    }

    public List<Message> accept(Long debtId) {
        responses.add(Messages.REGISTER_NOT);
        responses.add(Messages.REGISTER_USERNAME_SPEC);
        return responses;
    }

    public List<Message> kurs(String userId) {
        responses.add(Messages.REGISTER_NOT);
        responses.add(Messages.REGISTER_USERNAME_SPEC);
        return responses;
    }

}
