package com.ap_kelompok_1.debtreminder.handler.state;

import com.ap_kelompok_1.debtreminder.controller.chat_source.ChatSource;
import com.ap_kelompok_1.debtreminder.handler.flex_message.ProfileFlexMessage;
import com.ap_kelompok_1.debtreminder.handler.flex_message.ShowClaimFlexMessage;
import com.ap_kelompok_1.debtreminder.handler.flex_message.ShowDebtFlexMessage;
import com.ap_kelompok_1.debtreminder.handler.message.Messages;
import com.ap_kelompok_1.debtreminder.handler.state.helper.KursHelper;
import com.ap_kelompok_1.debtreminder.model.Debt;
import com.ap_kelompok_1.debtreminder.model.LineUser;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.Message;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Component
public class WaitingConfirmationState extends State {
    public static final String DB_COL_NAME = "WAITING_CONFIRMATION";

    private KursHelper kursHelper = new KursHelper();
    public WaitingConfirmationState() {
        this.responses = new ArrayList<>();
    }

    public void clearResponses() {
        this.responses.clear();
    }

    public List<Message> register(String userName, String userId, String displayName) {
        responses.add(Messages.REGISTER_ALREADY);
        return responses;
    }

    public List<Message> profile(String urlPicture, String displayName, String userName,
                                 String statusMessage, String userId) {
        ProfileFlexMessage profileFlexMessage = new ProfileFlexMessage(
                urlPicture,
                displayName,
                userName,
                statusMessage
        );
        responses.add(profileFlexMessage.get());
        return responses;
    }

    public List<Message> showDebt(String userId) {
        List<Debt> debtList = debtRepository.findDebtsByDebtorUserId(userId);
        ShowDebtFlexMessage showDebtFlexMessage = new ShowDebtFlexMessage(VerifiedDebt(debtList));
        responses.add(showDebtFlexMessage.get());
        return responses;
    }

    public List<Message> showClaim(String userId) {
        List<Debt> debtList = debtRepository.findDebtsByDebteeUserId(userId);
        ShowClaimFlexMessage showClaimFlexMessage = new ShowClaimFlexMessage(debtList);
        responses.add(showClaimFlexMessage.get());
        return responses;
    }

    public List<Message> bye(ChatSource chatSource, String userId)
            throws InterruptedException, ExecutionException {
        switch (chatSource.getType()) {
            case ChatSource.GROUP:
                this.lineMessagingClient.leaveGroup(
                        chatSource.getId()
                ).get();
                responses.add(Messages.BYE_GROUP);
                break;
            case ChatSource.ROOM:
                this.lineMessagingClient.leaveGroup(
                        chatSource.getId()
                ).get();
                responses.add(Messages.BYE_ROOM);
                break;
            case ChatSource.CHAT:
                responses.add(Messages.BYE_CHAT);
                break;
        }
        return responses;
    }

    public List<Message> help(String userId) {
        responses.add(Messages.HELP_TEXT);
        return responses;
    }

    public List<Message> addDebtee(String userId, String debteeId) {
        responses.add(Messages.WAITING_CONFIRMATION_CANT_ADD_NEW_DEBT);
        return responses;
    }

    public List<Message> addAmount(Long amount) {
        responses.add(Messages.WAITING_CONFIRMATION_CANT_ADD_NEW_DEBT);
        return responses;
    }

    public List<Message> addDueDate(Date dueDate) {
        responses.add(Messages.WAITING_CONFIRMATION_CANT_ADD_NEW_DEBT);
        return responses;
    }

    public List<Message> others(String command, String userId) {
        responses.add(Messages.OTHERS_TEXT);
        return responses;
    }

    public List<Message> cancel(String userId) {
        responses.add(Messages.CANCEL_SUCCESS);
        LineUser lineUser = lineUserRepository.findLineUserByUserId(userId);
        Debt debt = debtRepository.findUnverifiedDebtByDebtorUserid(userId);
        LineUser debtee = debt.getDebtee();
        lineUser.setState(RegisteredState.DB_COL_NAME);
        lineMessagingClient.pushMessage(
                new PushMessage(debtee.getUserId(), Messages.cancelDebtSuccess(lineUser.getUserName())));
        lineUserRepository.save(lineUser);
        debtRepository.deleteDebtByDebtId(debt.getDebtId());
        return responses;
    }

    public List<Message> remindDebtor(Long debtId) {
        return responses;
    }

    public List<Message> remindAllDebtor(String userId) {
        return responses;
    }

    public List<Message> settleDebt(Long debtId) {
        return responses;
    }

    public List<Message> settleAllDebt(String userId) {
        return responses;
    }

    private List<Debt> VerifiedDebt(List<Debt> debtList) {
        List<Debt> verifiedDebtList = new ArrayList<>();

        for (Debt debt : debtList) {
            if (debt.getVerified())
                verifiedDebtList.add(debt);
        }

        return verifiedDebtList;
    }

    public List<Message> kurs(String userId) {
        return kursHelper.getResponse();
    }

}
