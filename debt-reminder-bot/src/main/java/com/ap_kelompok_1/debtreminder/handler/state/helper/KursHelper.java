package com.ap_kelompok_1.debtreminder.handler.state.helper;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.ap_kelompok_1.debtreminder.handler.Handler;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.json.JSONObject;
import org.springframework.stereotype.Component;


@Component
public class KursHelper implements Handler {
    private List<Message> response = new ArrayList<>();
    Locale localeID = new Locale("in", "ID");
    NumberFormat formatter = NumberFormat.getCurrencyInstance(localeID);
    public void setResponses(List<Message> responses) {
        this.response = responses;
    }

    @Override
    public List<Message> getResponse() {
        try{
            String url = "https://api.exchangeratesapi.io/latest?base=USD&symbols=IDR";
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            JSONObject myResponse = new JSONObject(response.toString());
            JSONObject IDR1 = myResponse.getJSONObject("rates");
            Double IDR2 = IDR1.getDouble("IDR");
            String IDR3 = IDR2.toString();
            double amount = Double.parseDouble(IDR3);

            this.response.add(new TextMessage("1 USD equals "+formatter.format(amount) + "(IDR)"));
        }catch (Exception e){
            e.printStackTrace();
        }
        return this.response;
    }

    @Override
    public List<PushMessage> getPushMessages() {
        return null;
    }
}
