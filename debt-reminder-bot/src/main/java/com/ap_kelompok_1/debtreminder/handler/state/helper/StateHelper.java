package com.ap_kelompok_1.debtreminder.handler.state.helper;

import com.ap_kelompok_1.debtreminder.handler.state.AddDebtAmountState;
import com.ap_kelompok_1.debtreminder.handler.state.AddDueDateState;
import com.ap_kelompok_1.debtreminder.handler.state.RegisteredState;
import com.ap_kelompok_1.debtreminder.handler.state.RemindAllDebtConfirmationState;
import com.ap_kelompok_1.debtreminder.handler.state.RemindDebtConfirmationState;
import com.ap_kelompok_1.debtreminder.handler.state.SettleAllDebtConfirmationState;
import com.ap_kelompok_1.debtreminder.handler.state.SettleDebtConfirmationState;
import com.ap_kelompok_1.debtreminder.handler.state.State;
import com.ap_kelompok_1.debtreminder.handler.state.UnimplementedState;
import com.ap_kelompok_1.debtreminder.handler.state.UnregisteredState;
import com.ap_kelompok_1.debtreminder.handler.state.WaitingConfirmationState;
import com.ap_kelompok_1.debtreminder.repository.LineUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StateHelper {

    @Autowired
    LineUserRepository lineUserRepository;

    @Autowired
    private RegisteredState registeredState;

    @Autowired
    private UnregisteredState unregisteredState;

    @Autowired
    private UnimplementedState unimplementedState;

    @Autowired
    private AddDebtAmountState addDebtAmountState;

    @Autowired
    private AddDueDateState addDueDateState;

    @Autowired
    private RemindDebtConfirmationState remindDebtConfirmationState;

    @Autowired
    private SettleDebtConfirmationState settleDebtConfirmationState;

    @Autowired
    private RemindAllDebtConfirmationState remindAllDebtConfirmationState;

    @Autowired
    private SettleAllDebtConfirmationState settleAllDebtConfirmationState;

    @Autowired
    private WaitingConfirmationState waitingConfirmationState;

    public State toState(String stateString) {
        switch (stateString) {
            case AddDueDateState.DB_COL_NAME:
                return addDueDateState;
            case AddDebtAmountState.DB_COL_NAME:
                return addDebtAmountState;
            case RemindDebtConfirmationState.DB_COL_NAME:
                return remindDebtConfirmationState;
            case RemindAllDebtConfirmationState.DB_COL_NAME:
                return remindAllDebtConfirmationState;
            case SettleDebtConfirmationState.DB_COL_NAME:
                return settleDebtConfirmationState;
            case SettleAllDebtConfirmationState.DB_COL_NAME:
                return settleAllDebtConfirmationState;
            case RegisteredState.DB_COL_NAME:
                return registeredState;
            case UnregisteredState.DB_COL_NAME:
                return unregisteredState;
            case WaitingConfirmationState.DB_COL_NAME:
                return waitingConfirmationState;
            default:
                return unimplementedState;
        }
    }

    public State getUserState(String userId) {
        State state;
        if (lineUserRepository.isUserRegistered(userId)) {
            String stateString = lineUserRepository.findStateById(userId);
            state = toState(stateString);
        } else {
            state = unregisteredState;
        }
        state.clearResponses();
        return state;
    }
}
