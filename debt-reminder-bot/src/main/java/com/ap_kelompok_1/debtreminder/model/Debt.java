package com.ap_kelompok_1.debtreminder.model;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@Data
@Entity
@Table(name = "debt")
public class Debt extends AuditModel {
    @Id
    @Column(name = "debt_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long debtId;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "user_id", nullable = false)
    private LineUser debtee;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "user_id", nullable = false)
    private LineUser debtor;

    @Column(name = "debt_amount")
    private Long debtAmount;

    @Column(name = "due_date")
    private Date dueDate;

    @Column(name = "verified")
    private boolean verified;

    @Column(name = "remindable")
    private boolean remindable;

    @Column(name = "settleable")
    private boolean settleable;

    public Debt() {
    }

    public Debt(LineUser debtee, LineUser debtor, Long debtAmount, Date due_date) {
        this.debtee = debtee;
        this.debtor = debtor;
        this.debtAmount = debtAmount;
        this.dueDate = due_date;
        this.verified = false;
        this.remindable = false;
        this.settleable = false;
    }

    public Long getDebtAmount() {
        return debtAmount;
    }

    public void setDebtAmount(long debtAmount) {
        this.debtAmount = debtAmount;
    }

    public void setDebtAmount(Long debtAmount) {
        this.debtAmount = debtAmount;
    }

    public LineUser getDebtee() {
        return debtee;
    }

    public void setDebtee(LineUser debtee) {
        this.debtee = debtee;
    }

    public LineUser getDebtor() {
        return debtor;
    }

    public void setDebtor(LineUser debtor) {
        this.debtor = debtor;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public boolean getVerified() {
        return verified;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Long getDebtId() {
        return debtId;
    }

    public void setDebtId(Long debtId) {
        this.debtId = debtId;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public void setRemindable(boolean remindable) {
        this.remindable = remindable;
    }

    public void setSettleable(boolean settleable) {
        this.settleable = settleable;
    }
}
