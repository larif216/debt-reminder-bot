package com.ap_kelompok_1.debtreminder.model;

import com.ap_kelompok_1.debtreminder.handler.state.RegisteredState;
import com.ap_kelompok_1.debtreminder.handler.state.UnregisteredState;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

//TODO : TAMBAHIN DEBTEE KE SIAPA, AMOUNT KE SIAPA, DUE DATE KESIAPA

@EqualsAndHashCode
@Data
@Entity
@Table(name = "line_user")
public class LineUser extends AuditModel {
    @Id
    @Column(name = "user_id", unique = true, nullable = false)
    private String userId;

    @Column(name = "display_name")
    private String displayName;

    @Column(name = "user_name", unique = true)
    private String userName;

    @Column(name = "state")
    private String state = UnregisteredState.DB_COL_NAME;

    public LineUser() {
    }

    public LineUser(String userId, String displayName, String userName) {
        this.userId = userId;
        this.displayName = displayName;
        this.userName = userName;
        this.state = RegisteredState.DB_COL_NAME;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String userName) {
        this.displayName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String settingDebtState) {
        this.state = settingDebtState;
    }
}
