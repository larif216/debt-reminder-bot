package com.ap_kelompok_1.debtreminder.repository;

import com.ap_kelompok_1.debtreminder.model.Debt;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

public interface DebtRepository extends JpaRepository<Debt, Long> {
    @Query(value = "SELECT * FROM DEBT D, LINE_USER L WHERE DEBTEE_USER_ID = ?1 " +
        "AND D.DEBTEE_USER_ID = L.USER_ID", nativeQuery = true)
    List<Debt> findDebtsByDebteeUserId(String userId);

    @Query(value = "SELECT * FROM DEBT D, LINE_USER L WHERE DEBTOR_USER_ID = ?1 " +
        "AND D.DEBTOR_USER_ID = L.USER_ID", nativeQuery = true)
    List<Debt> findDebtsByDebtorUserId(String userId);

    @Query(value = "SELECT * FROM DEBT WHERE DEBT_ID = ?1 ", nativeQuery = true)
    Debt findDebtByDebtId(long debtId);

    @Query(value = "SELECT CASE WHEN COUNT(D) > 0 THEN true ELSE false END FROM DEBT D WHERE D.DEBT_ID = ?1", nativeQuery = true)
    boolean isDebtExist(long debtId);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM DEBT WHERE DEBT_ID = ?1", nativeQuery = true)
    void deleteDebtByDebtId(long debtId);

    @Query(value = "DELETE FROM DEBT WHERE DEBTEE_USER_ID = ?1", nativeQuery = true)
    void deleteDebtsByDebteeUserId(String userId);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM DEBT WHERE DEBTEE_USER_ID = ?1 AND VERIFIED=TRUE", nativeQuery = true)
    void deleteVerifiedDebtsByDebteeUserId(String userId);

    @Query(value = "SELECT * FROM DEBT WHERE VERIFIED=TRUE AND DEBTEE_USER_ID = ?1", nativeQuery = true)
    List<Debt> findVerifiedDebtsByDebteeUserId(String userId);

    @Query(value = "SELECT * FROM debt d WHERE verified = TRUE AND d.debtor_user_id = ?1", nativeQuery = true)
    List<Debt> findVerifiedDebtsByDebtorUserid(String userId);

    @Query(value = "SELECT * FROM debt d WHERE verified = FALSE AND d.debtor_user_id = ?1", nativeQuery = true)
    List<Debt> findUnverifiedDebtsByDebtorUserid(String userId);

    @Query(value = "SELECT * FROM debt d WHERE verified = FALSE AND d.debtor_user_id = ?1 LIMIT 1", nativeQuery = true)
    Debt findUnverifiedDebtByDebtorUserid(String userId);

    @Query(value = "SELECT * FROM debt d WHERE verified = FALSE AND d.debtee_user_id = ?1 LIMIT 1", nativeQuery = true)
    Debt findUnverifiedDebtByDebteeUserid(String userId);

    @Query(value = "SELECT * FROM debt d WHERE verified = FALSE AND debt_id = ?1", nativeQuery = true)
    Debt findUnverifiedDebtByDebtId(long debtId);

    @Query(value = "SELECT * FROM DEBT D WHERE remindable = TRUE AND D.DEBTEE_USER_ID = ?1", nativeQuery = true)
    List<Debt> findRemindableDebtsByDebteeId(String userId);

    @Query(value = "SELECT * FROM DEBT D WHERE settleable = TRUE AND D.DEBTEE_USER_ID = ?1", nativeQuery = true)
    Debt findSettleableDebtByDebteeId(String userId);
}
