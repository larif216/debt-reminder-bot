package com.ap_kelompok_1.debtreminder.repository;

import com.ap_kelompok_1.debtreminder.model.LineUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface LineUserRepository extends JpaRepository<LineUser, String> {
    @Query(value = "SELECT * FROM LINE_USER WHERE USER_ID = ?1", nativeQuery = true)
    LineUser findLineUserByUserId(String userID);

    @Query(value = "SELECT * FROM LINE_USER WHERE USER_NAME = ?1", nativeQuery = true)
    LineUser findLineUserByUserName(String name);

    @Query(value = "SELECT * FROM LINE_USER WHERE USER_NAME LIKE ?1", nativeQuery = true)
    LineUser findLineUserLikeUserName(String name);

    @Query(value = "SELECT user_name FROM LINE_USER WHERE USER_ID = ?1", nativeQuery = true)
    String findUserNameByUserId(String userID);

    @Query(value = "SELECT user_id FROM LINE_USER WHERE USER_NAME = ?1", nativeQuery = true)
    String findUserIdByUserName(String userName);

    @Query(value = "SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM LINE_USER c WHERE c.USER_ID = ?1", nativeQuery = true)
    boolean isUserRegistered(String userID);

    @Query(value = "SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM LINE_USER c WHERE c.USER_NAME = ?1", nativeQuery = true)
    boolean isUserNameUnique(String name);

    @Query(value = "SELECT state FROM LINE_USER WHERE USER_ID = ?1", nativeQuery = true)
    String findStateById(String userID);
}
