package com.ap_kelompok_1.debtreminder;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class DebtReminderApplicationTests {

    @Test
    public void test() {
        DebtReminderApplication.main(new String[] {
            "--spring.main.web-environment=false",
            "--spring.autoconfigure.exclude=blahblahblah",
            // Override any other environment properties according to your needs
        });
    }
}

