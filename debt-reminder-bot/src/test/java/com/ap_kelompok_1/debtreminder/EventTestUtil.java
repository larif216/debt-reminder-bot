package com.ap_kelompok_1.debtreminder;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.postback.PostbackContent;
import com.linecorp.bot.model.event.source.UserSource;
import java.time.Instant;
import java.util.HashMap;

public class EventTestUtil {

    private EventTestUtil() {

    }

    public static MessageEvent<TextMessageContent> createDummyTextMessage(String text,
        String userId) {
        return new MessageEvent<>("replyToken", new UserSource(userId),
            new TextMessageContent("id", text),
            Instant.parse("2018-01-01T00:00:00.000Z"));
    }

    public static PostbackEvent createDummyPostbackData(String data, String userId) {
        return new PostbackEvent("replyToken",
                new UserSource(userId),
                new PostbackContent(data, new HashMap<>()),
                Instant.parse("2018-01-01T00:00:00.000Z"));
    }
}
