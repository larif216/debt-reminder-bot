package com.ap_kelompok_1.debtreminder.controller;

import com.ap_kelompok_1.debtreminder.EventTestUtil;
import com.ap_kelompok_1.debtreminder.controller.chat_source.ChatSource;
import com.ap_kelompok_1.debtreminder.handler.ApplicationService;
import com.ap_kelompok_1.debtreminder.handler.UniversalHandler;
import com.ap_kelompok_1.debtreminder.handler.state.State;
import com.ap_kelompok_1.debtreminder.handler.state.helper.StateHelper;
import com.ap_kelompok_1.debtreminder.repository.LineUserRepository;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.GroupSource;
import com.linecorp.bot.model.event.source.RoomSource;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.event.source.UserSource;
import com.linecorp.bot.model.profile.UserProfileResponse;
import com.linecorp.bot.model.response.BotApiResponse;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DebtReminderControllerTest {

    @Spy
    @InjectMocks
    private DebtReminderController debtReminderController;

    @Mock
    private LineUserRepository lineUserRepository;

    @Mock
    private LineMessagingClient lineMessagingClient;

    @Mock
    private CompletableFuture<UserProfileResponse> cpuresp;

    @Mock
    private CompletableFuture<BotApiResponse> botresp;

    @Mock
    private StateHelper stateHelper;

    @Mock
    private State state;

    @Mock
    private UniversalHandler universalHandler;

    @Mock
    private ApplicationService applicationService;

    @Test
    public void contextLoads() throws Exception {
        assertThat(debtReminderController).isNotNull();
    }

    @Before
    public void setUp() throws ExecutionException, InterruptedException {
        UserProfileResponse userProfileResponse = new UserProfileResponse(
            "steven", "1", "file://stub", "hello");

        when(lineMessagingClient.getProfile("1")).thenReturn(cpuresp);
        when(lineMessagingClient.getProfile("1").get()).thenReturn(userProfileResponse);
        when(stateHelper.getUserState("1")).thenReturn(state);
        when(lineUserRepository.findUserIdByUserName("uci")).thenReturn("2");

        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        when(botresp.get()).thenReturn(null);
        //doNothing().when(universalHandler).setResponses(null);
        //doNothing().when(applicationService).setHandler(null);
    }

    @Test
    public void getUserDisplayNameTest() throws ExecutionException, InterruptedException {
        assertEquals(debtReminderController.getUserDisplayName("1"), "steven");
    }

    @Test
    public void getUrlPictureTest() throws InterruptedException, ExecutionException {
        assertEquals(debtReminderController.getUrlPicture("1"), "file://stub");
    }

    @Test
    public void getUsernameTest() {
        MessageEvent<TextMessageContent> messageEvent;
        messageEvent = EventTestUtil.createDummyTextMessage("/register ipman", "1");
        assertEquals(debtReminderController.getUserName(messageEvent), "ipman");
    }

    @Test
    public void getCommandTest() {
        MessageEvent<TextMessageContent> messageEvent;
        messageEvent = EventTestUtil.createDummyTextMessage("/register ipman", "1");
        assertEquals(debtReminderController.getCommand(messageEvent), "/register");
    }

    @Test
    public void getChatSourceTest() {
        Source roomSource = new RoomSource("1", "1");
        Source groupSource = new GroupSource("1", "2");
        Source userSource = new UserSource("1");

        assertEquals(debtReminderController.getChatSource(roomSource).getType(), ChatSource.ROOM);
        assertEquals(debtReminderController.getChatSource(groupSource).getType(), ChatSource.GROUP);
        assertEquals(debtReminderController.getChatSource(userSource).getType(), ChatSource.CHAT);
    }

    @Test
    public void testCallsProfile() throws Exception {
        MessageEvent<TextMessageContent> messageEvent;
        messageEvent = EventTestUtil.createDummyTextMessage("/profile", "1");

        debtReminderController.handleTextEvent(messageEvent);
        Mockito.verify(debtReminderController).handleTextEvent(messageEvent);
        Mockito.verify(state).profile("file://stub", "steven", null, "hello", "1");
    }

    @Test
    public void testCallsRegister() throws Exception {
        MessageEvent<TextMessageContent> messageEvent;
        messageEvent = EventTestUtil.createDummyTextMessage("/register ipman", "1");

        debtReminderController.handleTextEvent(messageEvent);
        Mockito.verify(debtReminderController).handleTextEvent(messageEvent);
        Mockito.verify(state).register("ipman", "1", "steven");
    }

    @Test
    public void testCallsDebt() throws Exception {
        MessageEvent<TextMessageContent> messageEvent;
        messageEvent = EventTestUtil.createDummyTextMessage("/debt uci", "1");

        debtReminderController.handleTextEvent(messageEvent);
        Mockito.verify(debtReminderController).handleTextEvent(messageEvent);
        Mockito.verify(state).addDebtee("1", "2");
    }

    @Test
    public void testCallsBye() throws Exception {
        MessageEvent<TextMessageContent> messageEvent;
        messageEvent = EventTestUtil.createDummyTextMessage("/bye", "1");

        debtReminderController.handleTextEvent(messageEvent);
        Mockito.verify(debtReminderController).handleTextEvent(messageEvent);
        Mockito.verify(state).bye(any(ChatSource.class), eq("1"));
    }

    @Test
    public void testCallsHelp() throws Exception {
        MessageEvent<TextMessageContent> messageEvent;
        messageEvent = EventTestUtil.createDummyTextMessage("/help", "1");

        debtReminderController.handleTextEvent(messageEvent);
        Mockito.verify(debtReminderController).handleTextEvent(messageEvent);
        Mockito.verify(state).help("1");
    }

    @Test
    public void testCallsCancel() throws Exception {
        MessageEvent<TextMessageContent> messageEvent;
        messageEvent = EventTestUtil.createDummyTextMessage("/cancel", "1");

        debtReminderController.handleTextEvent(messageEvent);
        Mockito.verify(debtReminderController).handleTextEvent(messageEvent);
        Mockito.verify(state).cancel("1");
    }

    @Test
    public void testCallsDefault() throws Exception {
        MessageEvent<TextMessageContent> messageEvent;
        messageEvent = EventTestUtil.createDummyTextMessage("/wkwk", "1");

        debtReminderController.handleTextEvent(messageEvent);
        Mockito.verify(debtReminderController).handleTextEvent(messageEvent);
        Mockito.verify(state).others("/wkwk", "1");
    }

    @Test
    public void testPostbackRemindDebt() {
        PostbackEvent postbackEvent =
                EventTestUtil.createDummyPostbackData("remindDebt 1", "1");
        debtReminderController.handlePostbackEvent(postbackEvent);
        Mockito.verify(debtReminderController).handlePostbackEvent(postbackEvent);
        Mockito.verify(state).remindDebtor(Long.parseLong("1"));
    }

    @Test
    public void testPostbackRemindAllDebt() {
        PostbackEvent postbackEvent =
                EventTestUtil.createDummyPostbackData("remindAllDebt", "1");
        debtReminderController.handlePostbackEvent(postbackEvent);
        Mockito.verify(debtReminderController).handlePostbackEvent(postbackEvent);
        Mockito.verify(state).remindAllDebtor("1");
    }

    @Test
    public void testPostbackSettleDebt() {
        PostbackEvent postbackEvent =
                EventTestUtil.createDummyPostbackData("settleDebt 1", "1");
        debtReminderController.handlePostbackEvent(postbackEvent);
        Mockito.verify(debtReminderController).handlePostbackEvent(postbackEvent);
        Mockito.verify(state).settleDebt(Long.parseLong("1"));
    }

    @Test
    public void testPostbackSettleAllDebt() {
        PostbackEvent postbackEvent =
                EventTestUtil.createDummyPostbackData("settleAllDebt", "1");
        debtReminderController.handlePostbackEvent(postbackEvent);
        Mockito.verify(debtReminderController).handlePostbackEvent(postbackEvent);
        Mockito.verify(state).settleAllDebt("1");
    }

    @Test
    public void testPostbackAcceptDebt() {
        PostbackEvent postbackEvent =
                EventTestUtil.createDummyPostbackData("acceptDebt 1", "1");
        debtReminderController.handlePostbackEvent(postbackEvent);
        Mockito.verify(debtReminderController).handlePostbackEvent(postbackEvent);
        Mockito.verify(state).accept(Long.parseLong("1"));
    }

    @Test
    public void testPostbackRejectDebt() {
        PostbackEvent postbackEvent =
                EventTestUtil.createDummyPostbackData("rejectDebt 1", "1");
        debtReminderController.handlePostbackEvent(postbackEvent);
        Mockito.verify(debtReminderController).handlePostbackEvent(postbackEvent);
        Mockito.verify(state).reject(Long.parseLong("1"));
    }
}
