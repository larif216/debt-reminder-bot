package com.ap_kelompok_1.debtreminder.controller.chat_source;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ChatSourceTest {

    @Mock
    private ChatSource chatSource;

    @Test
    public void contextLoads() throws Exception {
        assertThat(chatSource).isNotNull();
    }
}
