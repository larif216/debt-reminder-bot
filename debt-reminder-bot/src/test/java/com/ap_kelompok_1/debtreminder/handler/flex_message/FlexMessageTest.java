package com.ap_kelompok_1.debtreminder.handler.flex_message;

import com.ap_kelompok_1.debtreminder.handler.message.Messages;
import com.ap_kelompok_1.debtreminder.model.Debt;
import com.ap_kelompok_1.debtreminder.model.LineUser;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class FlexMessageTest {

    @Test
    public void contextLoads() throws Exception {
    }

    @Mock
    LineUser user1, user2;

    @Test
    public void panggil(){
        Debt debt = new Debt(user1, user2, Long.parseLong("30000"),new Date());
        debt.setDebtId(Long.parseLong("3"));

        List<Debt> debtList = new ArrayList<>();
        ShowDebtFlexMessage showDebtFlexMessage2 = new ShowDebtFlexMessage(debtList);
        showDebtFlexMessage2.get();

        ShowClaimFlexMessage showClaimFlexMessage2 = new ShowClaimFlexMessage(debtList);
        showClaimFlexMessage2.get();

        debtList.add(debt);

        ShowClaimFlexMessage showClaimFlexMessage = new ShowClaimFlexMessage(debtList);
        showClaimFlexMessage.get();

        ShowDebtFlexMessage showDebtFlexMessage = new ShowDebtFlexMessage(debtList);
        showDebtFlexMessage.get();

        ProfileFlexMessage profileFlexMessage = new ProfileFlexMessage(
                "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png",
                "",
                "Upi",
                ""
        );
        profileFlexMessage.get();

        ProfileFlexMessage profileFlexMessage2 = new ProfileFlexMessage(
                "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png",
                "",
                "Upi",
                null
        );
        profileFlexMessage2.get();

        AcceptRejectFlexMessage acceptRejectFlexMessage = new AcceptRejectFlexMessage(debt);
        acceptRejectFlexMessage.get();

        assertEquals(true, true);
    }
}