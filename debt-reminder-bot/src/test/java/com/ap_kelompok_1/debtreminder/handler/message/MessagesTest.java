package com.ap_kelompok_1.debtreminder.handler.message;

import com.ap_kelompok_1.debtreminder.model.Debt;
import com.ap_kelompok_1.debtreminder.model.LineUser;
import java.util.Date;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class MessagesTest {
    @Spy
    @InjectMocks
    Messages messages;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    Debt debt;

    @Test
    public void contextLoads() throws Exception {
        assertThat(messages).isNotNull();
        when(debt.getDebtor().getUserName()).thenReturn("123");
    }

    @Test
    public void panggil(){

        messages.verifyYourDebt(debt);
        messages.remindDebtSuccess(debt);
        messages.confirmRemindDebt(debt);
        messages.remindDebt(debt);
        messages.settleDebt(debt);
        messages.settleDebtSuccess(debt);
        messages.cancelDebtSuccess("x");
        messages.confirmSettleDebt(debt);
        assertEquals(true, true);
    }
}
