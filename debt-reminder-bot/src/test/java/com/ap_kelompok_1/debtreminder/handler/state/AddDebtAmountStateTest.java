package com.ap_kelompok_1.debtreminder.handler.state;

import com.ap_kelompok_1.debtreminder.controller.chat_source.ChatSource;
import com.ap_kelompok_1.debtreminder.handler.message.Messages;
import com.ap_kelompok_1.debtreminder.model.Debt;
import com.ap_kelompok_1.debtreminder.model.LineUser;
import com.ap_kelompok_1.debtreminder.repository.DebtRepository;
import com.ap_kelompok_1.debtreminder.repository.LineUserRepository;
import com.linecorp.bot.model.message.Message;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AddDebtAmountStateTest {

    @Mock
    LineUserRepository lineUserRepository;
//    @Mock
//    LineMessagingClient lineMessagingClient;
    @Mock
    DebtRepository debtRepository;
    @InjectMocks
    private AddDebtAmountState addDebtAmountState;
    @Mock
    private List<Message> messages;

    @Test
    public void contextLoads() throws Exception {
        assertThat(addDebtAmountState).isNotNull();
    }

    @Before
    public void setUp() {
        LineUser userUci = new LineUser("2", "uciuci", "uciuci");
        LineUser userIpman = new LineUser("1", "ipman", "ipman");
        Debt debtIpman = new Debt(userIpman, userUci, Long.parseLong("300"), new Date());
        debtIpman.setDebtId(Long.parseLong("1"));

        userIpman.setState(AddDebtAmountState.DB_COL_NAME);
        when(lineUserRepository.findLineUserByUserId("1")).thenReturn(
            userIpman
        );
        when(debtRepository.findUnverifiedDebtByDebtorUserid("1")).thenReturn(
            debtIpman
        );
        doNothing().when(debtRepository).deleteDebtByDebtId(1);
    }

    @Test
    public void clearResponsesTest() {
        addDebtAmountState.clearResponses();
    }

    @Test
    public void registerTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        assertEquals(addDebtAmountState.register("1", "1", "1"), messages);
    }

    @Test
    public void profileTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        assertEquals(addDebtAmountState.profile("1", "1", "1", "1", "1"), messages);
    }

    @Test
    public void showDebtTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        assertEquals(addDebtAmountState.showDebt("1"), messages);
    }

    @Test
    public void showClaimTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        assertEquals(addDebtAmountState.showClaim("1"), messages);
    }

    @Test
    public void byeTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        assertEquals(addDebtAmountState.bye(new ChatSource(), "1"), messages);
    }

    @Test
    public void helpTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        assertEquals(addDebtAmountState.help("1"), messages);
    }

    @Test
    public void addDebteeTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        assertEquals(addDebtAmountState.addDebtee("1", "1"), messages);
    }

    @Test
    public void addAmountTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        assertEquals(addDebtAmountState.addAmount(Long.parseLong("1")), messages);
    }

    @Test
    public void addDueDateTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        assertEquals(addDebtAmountState.addDueDate(new Date()), messages);
    }

    @Test
    public void othersTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        assertEquals(addDebtAmountState.others("1", "1"), messages);
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        assertEquals(addDebtAmountState.others("0", "1"), messages);
    }

    @Test
    public void cancelTest() {
        messages.clear();
        messages.add(Messages.CANCEL_SUCCESS);
        assertEquals(addDebtAmountState.cancel("1"), messages);
    }

    @Test
    public void remindDebtorTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        assertEquals(addDebtAmountState.remindDebtor(Long.parseLong("1")), messages);
    }

    @Test
    public void remindAllDebtorTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        assertEquals(addDebtAmountState.remindAllDebtor("1"), messages);
    }

    @Test
    public void settleDebtTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        assertEquals(addDebtAmountState.settleDebt(Long.parseLong("1")), messages);
    }

    @Test
    public void settleAllDebtTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBTEE_SUCCESS);
        assertEquals(addDebtAmountState.settleAllDebt("1"), messages);
    }

//    @Test
//    public void rejectTest() {
//        messages.clear();
//        assertEquals(addDebtAmountState.reject("2"), messages);
//    }
//
//    @Test
//    public void acceptTest() {
//        messages.clear();
//        assertEquals(addDebtAmountState.accept("2"), messages);
//    }

}
