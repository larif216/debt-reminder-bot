package com.ap_kelompok_1.debtreminder.handler.state;

import com.ap_kelompok_1.debtreminder.controller.chat_source.ChatSource;
import com.ap_kelompok_1.debtreminder.handler.message.Messages;
import com.ap_kelompok_1.debtreminder.repository.DebtRepository;
import com.ap_kelompok_1.debtreminder.repository.LineUserRepository;
import com.linecorp.bot.model.message.Message;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class AddDueDateStateTest {
    @Mock
    LineUserRepository lineUserRepository;
    @Mock
    DebtRepository debtRepository;
    @InjectMocks
    private AddDueDateState addDueDateState;
    @Mock
    private List<Message> messages;

//    @Before
//    public void setUp() {
//        LineUser userUci = new LineUser("2", "uciuci", "uciuci");
//        LineUser userIpman = new LineUser("1", "ipman", "ipman");
//        Debt debtIpman = new Debt(userIpman, userUci, Long.parseLong("300"), new Date());
//        debtIpman.setDebtId(Long.parseLong("1"));
//
//        userIpman.setState(AddDebtAmountState.DB_COL_NAME);
//        when(lineUserRepository.findLineUserByUserId("1")).thenReturn(
//                userIpman
//        );
//        when(debtRepository.findUnverifiedDebtByDebtorUserid("1")).thenReturn(
//                debtIpman
//        );
//        doNothing().when(debtRepository).deleteDebtByDebtId(1);
//    }

    @Test
    public void clearResponsesTest() {
        addDueDateState.clearResponses();
    }

    @Test
    public void registerTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        assertEquals(addDueDateState.register("1", "1", "1"), messages);
    }

    @Test
    public void profileTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        assertEquals(addDueDateState.profile("1", "1", "1", "1", "1"), messages);
    }

    @Test
    public void showDebtTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        assertEquals(addDueDateState.showDebt("1"), messages);
    }

    @Test
    public void showClaimTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        assertEquals(addDueDateState.showClaim("1"), messages);
    }

    @Test
    public void byeTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        assertEquals(addDueDateState.bye(new ChatSource(), "1"), messages);
    }

    @Test
    public void helpTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        assertEquals(addDueDateState.help("1"), messages);
    }

    @Test
    public void addDebteeTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        assertEquals(addDueDateState.addDebtee("1", "1"), messages);
    }

    @Test
    public void addAmountTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        assertEquals(addDueDateState.addAmount(Long.parseLong("1")), messages);
    }

    @Test
    public void addDueDateTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        assertEquals(addDueDateState.addDueDate(new Date()), messages);
    }

    @Test
    public void othersTest() {

    }

    @Test
    public void cancelTest() {
    }

    @Test
    public void remindDebtorTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        assertEquals(addDueDateState.remindDebtor(Long.parseLong("1")), messages);
    }

    @Test
    public void remindAllDebtorTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        assertEquals(addDueDateState.remindAllDebtor("1"), messages);
    }

    @Test
    public void settleDebtTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        assertEquals(addDueDateState.settleDebt(Long.parseLong("1")), messages);
    }

    @Test
    public void settleAllDebtTest() {
        messages.clear();
        messages.add(Messages.DEBT_ADD_DEBT_AMOUNT_SUCCESS);
        assertEquals(addDueDateState.settleAllDebt("1"), messages);
    }

    @Test
    public void reject() {
    }

    @Test
    public void accept() {
    }
}