package com.ap_kelompok_1.debtreminder.handler.state;

import com.ap_kelompok_1.debtreminder.model.Debt;
import com.ap_kelompok_1.debtreminder.model.LineUser;
import com.ap_kelompok_1.debtreminder.repository.DebtRepository;
import com.ap_kelompok_1.debtreminder.repository.LineUserRepository;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.message.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

//@RunWith(MockitoJUnitRunner.class)
public class RegisteredStateTest {
    //    @Mock
    //    LineUserRepository lineUserRepository;
    //    @Mock
    //    DebtRepository debtRepository;
    //    @Mock
    //    private List<Message> messages;
    //    @Mock
    //    LineMessagingClient lineMessagingClient;
    //    @InjectMocks
    //    private RegisteredState registeredState;
    //
    //    @Test
    //    public void contextLoads() {
    //        assertThat(registeredState).isNotNull();
    //    }
    //
    //    @Before
    //    public void setUp() {
    //        LineUser userUci = new LineUser("2", "uciuci", "uciuci");
    //        LineUser userIpman = new LineUser("1", "ipman", "ipman");
    //        Debt debtIpman = new Debt(userIpman, userUci, Long.parseLong("300"), new Date());
    //        Debt debtUci1 = new Debt(userUci, userIpman, Long.parseLong("100"), new Date());
    //        Debt debtUci2 = new Debt(userUci, userIpman, Long.parseLong("200"), new Date());
    //        debtIpman.setRemindable(true);
    //        debtUci1.setRemindable(true);
    //        debtUci2.setRemindable(true);
    //        debtIpman.setDebtId(Long.parseLong("1"));
    //        userIpman.setState(registeredState.DB_COL_NAME);
    //        List<Debt> debtsIpman = new ArrayList<>();
    //        debtsIpman.add(debtIpman);
    //        when(debtRepository.findVerifiedDebtsByDebteeUserId("1")).thenReturn(
    //                debtsIpman
    //        );
    //        when(lineUserRepository.findLineUserByUserId("1")).thenReturn(
    //                userIpman
    //        );
    //
    //    }


    @Test
    public void clearResponses() {
    }

    @Test
    public void showUsernameGroup() {
    }

    @Test
    public void register() {
    }

    @Test
    public void profile() {
    }

    @Test
    public void showDebt() {
    }

    @Test
    public void showClaim() {
    }

    @Test
    public void bye() {
    }

    @Test
    public void help() {
    }

    @Test
    public void addDebtee() {
    }

    @Test
    public void addAmount() {
    }

    @Test
    public void addDueDate() {
    }

    @Test
    public void others() {
    }

    @Test
    public void cancel() {
    }

    @Test
    public void remindDebtor() {
    }

    @Test
    public void remindAllDebtor() {
    }

    @Test
    public void settleDebt() {
    }

    @Test
    public void settleAllDebt() {
    }

    @Test
    public void reject() {
    }

    @Test
    public void accept() {
    }
}