package com.ap_kelompok_1.debtreminder.handler.state;

import com.ap_kelompok_1.debtreminder.controller.chat_source.ChatSource;
import com.ap_kelompok_1.debtreminder.handler.message.Messages;
import com.ap_kelompok_1.debtreminder.model.Debt;
import com.ap_kelompok_1.debtreminder.model.LineUser;
import com.ap_kelompok_1.debtreminder.repository.DebtRepository;
import com.ap_kelompok_1.debtreminder.repository.LineUserRepository;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.message.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RemindAllDebtConfirmationStateTest {
    @Mock
    LineUserRepository lineUserRepository;
    @Mock
    DebtRepository debtRepository;
    @Mock
    private List<Message> messages;
    @Mock
    LineMessagingClient lineMessagingClient;
    @InjectMocks
    private RemindAllDebtConfirmationState remindAllDebtConfirmationState;

    @Test
    public void contextLoads() {
        assertThat(remindAllDebtConfirmationState).isNotNull();
    }

    @Before
    public void setUp() {
        LineUser userUci = new LineUser("2", "uciuci", "uciuci");
        LineUser userIpman = new LineUser("1", "ipman", "ipman");
        Debt debtIpman = new Debt(userIpman, userUci, Long.parseLong("300"), new Date());
        Debt debtUci1 = new Debt(userUci, userIpman, Long.parseLong("100"), new Date());
        Debt debtUci2 = new Debt(userUci, userIpman, Long.parseLong("200"), new Date());
        debtIpman.setRemindable(true);
        debtUci1.setRemindable(true);
        debtUci2.setRemindable(true);
        debtIpman.setDebtId(Long.parseLong("1"));
        userIpman.setState(remindAllDebtConfirmationState.DB_COL_NAME);
        List<Debt> debtsIpman = new ArrayList<>();
        debtsIpman.add(debtIpman);
        when(debtRepository.findVerifiedDebtsByDebteeUserId("1")).thenReturn(
                debtsIpman
        );
        when(lineUserRepository.findLineUserByUserId("1")).thenReturn(
                userIpman
        );

    }

    @Test
    public void clearResponsesTest() {
        remindAllDebtConfirmationState.clearResponses();
    }

    @Test
    public void registerTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindAllDebtConfirmationState.register("1", "1", "1"), messages);
    }

    @Test
    public void profileTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindAllDebtConfirmationState.profile("1", "1", "1", "1", "1"), messages);
    }

    @Test
    public void showDebtTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindAllDebtConfirmationState.showDebt("1"), messages);
    }

    @Test
    public void showClaimTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindAllDebtConfirmationState.showClaim("1"), messages);
    }

    @Test
    public void byeTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindAllDebtConfirmationState.bye(new ChatSource(), "1"), messages);
    }

    @Test
    public void helpTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindAllDebtConfirmationState.help("1"), messages);
    }

    @Test
    public void addDebteeTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindAllDebtConfirmationState.addDebtee("1", "1"), messages);
    }

    @Test
    public void addAmountTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindAllDebtConfirmationState.addAmount(Long.parseLong("1")), messages);
    }

    @Test
    public void addDueDateTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindAllDebtConfirmationState.addDueDate(new Date()), messages);
    }

    @Test
    public void othersTest() {
        messages.clear();
        messages.add(Messages.REMIND_ALL_DEBT_SUCCESS);
        assertEquals(remindAllDebtConfirmationState.others("/yes", "1"), messages);

        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindAllDebtConfirmationState.others("/another", "1"), messages);
    }

    @Test
    public void cancelTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_CANCEL);
        assertEquals(remindAllDebtConfirmationState.cancel("1"), messages);
    }

    @Test
    public void remindDebtorTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindAllDebtConfirmationState.remindDebtor(Long.parseLong("1")), messages);
    }

    @Test
    public void remindAllDebtorTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindAllDebtConfirmationState.remindAllDebtor("1"), messages);
    }

    @Test
    public void settleDebtTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindAllDebtConfirmationState.settleDebt(Long.parseLong("1")), messages);
    }

    @Test
    public void settleAllDebtTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindAllDebtConfirmationState.settleAllDebt("1"), messages);
    }

    @Test
    public void reject() {
    }

    @Test
    public void accept() {
    }
}
