package com.ap_kelompok_1.debtreminder.handler.state;

import com.ap_kelompok_1.debtreminder.handler.message.Messages;
import com.ap_kelompok_1.debtreminder.controller.chat_source.ChatSource;
import com.ap_kelompok_1.debtreminder.model.Debt;
import com.ap_kelompok_1.debtreminder.model.LineUser;
import com.ap_kelompok_1.debtreminder.repository.DebtRepository;
import com.ap_kelompok_1.debtreminder.repository.LineUserRepository;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.message.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RemindDebtConfirmationStateTest {

    @Mock
    LineUserRepository lineUserRepository;
    @Mock
    DebtRepository debtRepository;
    @Mock
    private List<Message> messages;
    @Mock
    LineMessagingClient lineMessagingClient;
    @InjectMocks
    private RemindDebtConfirmationState remindDebtConfirmationState;

    @Test
    public void contextLoads() {
        assertThat(remindDebtConfirmationState).isNotNull();
    }

    @Before
    public void setUp() {
        LineUser userUci = new LineUser("2", "uciuci", "uciuci");
        LineUser userIpman = new LineUser("1", "ipman", "ipman");
        Debt debtIpman = new Debt(userIpman, userUci, Long.parseLong("300"), new Date());
        Debt debtUci1 = new Debt(userUci, userIpman, Long.parseLong("100"), new Date());
        Debt debtUci2 = new Debt(userUci, userIpman, Long.parseLong("200"), new Date());
        debtIpman.setDebtId(Long.parseLong("1"));
        debtIpman.setRemindable(true);
        debtUci1.setRemindable(true);
        debtUci2.setRemindable(true);
        userIpman.setState(remindDebtConfirmationState.DB_COL_NAME);
        userUci.setState(remindDebtConfirmationState.DB_COL_NAME);
        List<Debt> debtsIpman = new ArrayList<>();
        debtsIpman.add(debtIpman);
        when(debtRepository.findRemindableDebtsByDebteeId("1")).thenReturn(
                debtsIpman
        );
        List<Debt> debtsUci = new ArrayList<>();
        debtsUci.add(debtUci1);
        debtsUci.add(debtUci2);
        when(debtRepository.findRemindableDebtsByDebteeId("2")).thenReturn(
                debtsUci
        );
    }

    @Test
    public void clearResponsesTest() {
        remindDebtConfirmationState.clearResponses();
    }

    @Test
    public void registerTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindDebtConfirmationState.register("1", "1", "1"), messages);
    }

    @Test
    public void profileTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindDebtConfirmationState.profile("1", "1", "1", "1", "1"), messages);
    }

    @Test
    public void showDebtTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindDebtConfirmationState.showDebt("1"), messages);
    }

    @Test
    public void showClaimTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindDebtConfirmationState.showClaim("1"), messages);
    }

    @Test
    public void byeTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindDebtConfirmationState.bye(new ChatSource(), "1"), messages);
    }

    @Test
    public void helpTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindDebtConfirmationState.help("1"), messages);
    }

    @Test
    public void addDebteeTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindDebtConfirmationState.addDebtee("1", "1"), messages);
    }

    @Test
    public void addAmountTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindDebtConfirmationState.addAmount(Long.parseLong("1")), messages);
    }

    @Test
    public void addDueDateTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindDebtConfirmationState.addDueDate(new Date()), messages);
    }

    @Test
    public void othersTest() {
        messages.clear();
        List<Debt> debtsIpman = debtRepository.findRemindableDebtsByDebteeId("1");
        Debt debtIpman = debtsIpman.get(0);
        messages.add(Messages.remindDebtSuccess(debtIpman));
        assertEquals(remindDebtConfirmationState.others("/yes", "1"), messages);

        messages.clear();
        messages.add(Messages.REMIND_DEBT_ERROR);
        assertEquals(remindDebtConfirmationState.others("/yes", "2"), messages);

        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindDebtConfirmationState.others("/another", "1"), messages);
    }

    @Test
    public void cancelTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_CANCEL);
        assertEquals(remindDebtConfirmationState.cancel("1"), messages);

        messages.clear();
        messages.add(Messages.REMIND_DEBT_ERROR);
        assertEquals(remindDebtConfirmationState.cancel("2"), messages);
    }

    @Test
    public void remindDebtorTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindDebtConfirmationState.remindDebtor(Long.parseLong("1")), messages);
    }

    @Test
    public void remindAllDebtorTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindDebtConfirmationState.remindAllDebtor("1"), messages);
    }

    @Test
    public void settleDebtTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindDebtConfirmationState.settleDebt(Long.parseLong("1")), messages);
    }

    @Test
    public void settleAllDebtTest() {
        messages.clear();
        messages.add(Messages.REMIND_DEBT_CONFIRM_FAIL);
        assertEquals(remindDebtConfirmationState.settleAllDebt("1"), messages);
    }
}
