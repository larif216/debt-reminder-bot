package com.ap_kelompok_1.debtreminder.handler.state;

import com.ap_kelompok_1.debtreminder.controller.chat_source.ChatSource;
import com.ap_kelompok_1.debtreminder.handler.message.Messages;
import com.ap_kelompok_1.debtreminder.model.Debt;
import com.ap_kelompok_1.debtreminder.model.LineUser;
import com.ap_kelompok_1.debtreminder.repository.DebtRepository;
import com.ap_kelompok_1.debtreminder.repository.LineUserRepository;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.message.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SettleAllDebtConfirmationStateTest {
    @Mock
    LineUserRepository lineUserRepository;
    @Mock
    DebtRepository debtRepository;
    @Mock
    private List<Message> messages;
    @Mock
    LineMessagingClient lineMessagingClient;
    @InjectMocks
    private SettleAllDebtConfirmationState settleAllDebtConfirmationState;

    @Test
    public void contextLoads() {
        assertThat(settleAllDebtConfirmationState).isNotNull();
    }

    @Before
    public void setUp() {
        LineUser userUci = new LineUser("2", "uciuci", "uciuci");
        LineUser userIpman = new LineUser("1", "ipman", "ipman");
        Debt debtIpman = new Debt(userIpman, userUci, Long.parseLong("300"), new Date());
        debtIpman.setDebtId(Long.parseLong("1"));
        userIpman.setState(settleAllDebtConfirmationState.DB_COL_NAME);
        List<Debt> debtsIpman = new ArrayList<>();
        debtsIpman.add(debtIpman);
        when(debtRepository.findVerifiedDebtsByDebteeUserId("1")).thenReturn(
                debtsIpman
        );
        when(lineUserRepository.findLineUserByUserId("1")).thenReturn(
                userIpman
        );
        doNothing().when(debtRepository).deleteVerifiedDebtsByDebteeUserId("1");
    }

    @Test
    public void clearResponsesTest() {
        settleAllDebtConfirmationState.clearResponses();
    }

    @Test
    public void registerTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleAllDebtConfirmationState.register("1", "1", "1"), messages);
    }

    @Test
    public void profileTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleAllDebtConfirmationState.profile("1", "1", "1", "1", "1"), messages);
    }

    @Test
    public void showDebtTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleAllDebtConfirmationState.showDebt("1"), messages);
    }

    @Test
    public void showClaimTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleAllDebtConfirmationState.showClaim("1"), messages);
    }

    @Test
    public void byeTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleAllDebtConfirmationState.bye(new ChatSource(), "1"), messages);
    }

    @Test
    public void helpTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleAllDebtConfirmationState.help("1"), messages);
    }

    @Test
    public void addDebteeTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleAllDebtConfirmationState.addDebtee("1", "1"), messages);
    }

    @Test
    public void addAmountTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleAllDebtConfirmationState.addAmount(Long.parseLong("1")), messages);
    }

    @Test
    public void addDueDateTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleAllDebtConfirmationState.addDueDate(new Date()), messages);
    }

    @Test
    public void othersTest() {
        messages.clear();
        messages.add(Messages.SETTLE_ALL_DEBT_SUCCESS);
        assertEquals(settleAllDebtConfirmationState.others("/yes", "1"), messages);
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleAllDebtConfirmationState.others("/another", "1"), messages);
    }

    @Test
    public void cancelTest() {
        messages.clear();
        messages.add(Messages.SETTLE_ALL_DEBT_CANCEL);
        assertEquals(settleAllDebtConfirmationState.cancel("1"), messages);
    }

    @Test
    public void remindDebtorTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleAllDebtConfirmationState.remindDebtor(Long.parseLong("1")), messages);
    }

    @Test
    public void remindAllDebtorTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleAllDebtConfirmationState.remindAllDebtor("1"), messages);
    }

    @Test
    public void settleDebtTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleAllDebtConfirmationState.settleDebt(Long.parseLong("1")), messages);
    }

    @Test
    public void settleAllDebtTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleAllDebtConfirmationState.settleAllDebt("1"), messages);
    }
}
