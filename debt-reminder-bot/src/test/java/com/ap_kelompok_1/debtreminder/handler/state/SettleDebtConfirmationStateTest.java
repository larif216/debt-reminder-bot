package com.ap_kelompok_1.debtreminder.handler.state;

import com.ap_kelompok_1.debtreminder.controller.chat_source.ChatSource;
import com.ap_kelompok_1.debtreminder.handler.message.Messages;
import com.ap_kelompok_1.debtreminder.model.Debt;
import com.ap_kelompok_1.debtreminder.model.LineUser;
import com.ap_kelompok_1.debtreminder.repository.DebtRepository;
import com.ap_kelompok_1.debtreminder.repository.LineUserRepository;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.message.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SettleDebtConfirmationStateTest {
    @Mock
    LineUserRepository lineUserRepository;
    @Mock
    DebtRepository debtRepository;
    @Mock
    private List<Message> messages;
    @Mock
    LineMessagingClient lineMessagingClient;
    @InjectMocks
    private SettleDebtConfirmationState settleDebtConfirmationState;

    @Test
    public void contextLoads() {
        assertThat(settleDebtConfirmationState).isNotNull();
    }

    @Before
    public void setUp() {
        LineUser userUci = new LineUser("2", "uciuci", "uciuci");
        LineUser userIpman = new LineUser("1", "ipman", "ipman");
        Debt debtIpman = new Debt(userIpman, userUci, Long.parseLong("300"), new Date());
        debtIpman.setDebtId(Long.parseLong("1"));
        userIpman.setState(SettleDebtConfirmationState.DB_COL_NAME);
        when(debtRepository.findSettleableDebtByDebteeId("1")).thenReturn(
                debtIpman
        );
        when(lineUserRepository.findLineUserByUserId("1")).thenReturn(
                userIpman
        );
        doNothing().when(debtRepository).deleteDebtByDebtId(1);
    }

    @Test
    public void clearResponsesTest() {
        settleDebtConfirmationState.clearResponses();
    }

    @Test
    public void registerTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleDebtConfirmationState.register("1", "1", "1"), messages);
    }

    @Test
    public void profileTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleDebtConfirmationState.profile("1", "1", "1", "1", "1"), messages);
    }

    @Test
    public void showDebtTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleDebtConfirmationState.showDebt("1"), messages);
    }

    @Test
    public void showClaimTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleDebtConfirmationState.showClaim("1"), messages);
    }

    @Test
    public void byeTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleDebtConfirmationState.bye(new ChatSource(), "1"), messages);
    }

    @Test
    public void helpTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleDebtConfirmationState.help("1"), messages);
    }

    @Test
    public void addDebteeTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleDebtConfirmationState.addDebtee("1", "1"), messages);
    }

    @Test
    public void addAmountTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleDebtConfirmationState.addAmount(Long.parseLong("1")), messages);
    }

    @Test
    public void addDueDateTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleDebtConfirmationState.addDueDate(new Date()), messages);
    }

    @Test
    public void othersTest() {
        messages.clear();
        Debt debtIpman = debtRepository.findSettleableDebtByDebteeId("1");
        messages.add(Messages.settleDebtSuccess(debtIpman));
        assertEquals(settleDebtConfirmationState.others("/yes", "1"), messages);
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleDebtConfirmationState.others("/another", "1"), messages);
    }

    @Test
    public void cancelTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CANCEL);
        assertEquals(settleDebtConfirmationState.cancel("1"), messages);
    }

    @Test
    public void remindDebtorTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleDebtConfirmationState.remindDebtor(Long.parseLong("1")), messages);
    }

    @Test
    public void remindAllDebtorTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleDebtConfirmationState.remindAllDebtor("1"), messages);
    }

    @Test
    public void settleDebtTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleDebtConfirmationState.settleDebt(Long.parseLong("1")), messages);
    }

    @Test
    public void settleAllDebtTest() {
        messages.clear();
        messages.add(Messages.SETTLE_DEBT_CONFIRM_FAIL);
        assertEquals(settleDebtConfirmationState.settleAllDebt("1"), messages);
    }
}
