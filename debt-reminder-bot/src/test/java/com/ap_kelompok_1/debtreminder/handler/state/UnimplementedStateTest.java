package com.ap_kelompok_1.debtreminder.handler.state;

import com.ap_kelompok_1.debtreminder.controller.chat_source.ChatSource;
import com.linecorp.bot.model.message.Message;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class UnimplementedStateTest {

    @InjectMocks
    private UnimplementedState unimplementedState;

    @Mock
    private List<Message> messages;

    @Test
    public void contextLoads() throws Exception {
        assertThat(unimplementedState).isNotNull();
    }

    @Test
    public void clearResponsesTest() {
        unimplementedState.clearResponses();
    }

    @Test
    public void registerTest() {
        assertEquals(unimplementedState.register("1", "1", "1"), messages);
    }

    @Test
    public void profileTest() {
        assertEquals(unimplementedState.profile("1", "1", "1", "1", "1"), messages);
    }

    @Test
    public void showDebtTest() {
        assertEquals(unimplementedState.showDebt("1"), messages);
    }

    public void showClaimTest() {
        assertEquals(unimplementedState.showClaim("1"), messages);
    }


    @Test
    public void byeTest() {
        assertEquals(unimplementedState.bye(new ChatSource(), "1"), messages);
    }

    @Test
    public void helpTest() {
        assertEquals(unimplementedState.help("1"), messages);
    }

    @Test
    public void addDebteeTest() {
        assertEquals(unimplementedState.addDebtee("1", "1"), messages);
    }

    @Test
    public void addAmountTest() {
        assertEquals(unimplementedState.addAmount(Long.parseLong("1")), messages);
    }

    @Test
    public void addDueDateTest() {
        assertEquals(unimplementedState.addDueDate(new Date()), messages);
    }

    @Test
    public void othersTest() {
        assertEquals(unimplementedState.others("1", "1"), messages);
    }

    @Test
    public void cancelTest() {
        assertEquals(unimplementedState.cancel("1"), messages);
    }

    @Test
    public void settleDebtTest() {
        assertEquals(unimplementedState.settleDebt(Long.parseLong("1")), messages);
    }

    @Test
    public void remindDebtTest() {
        assertEquals(unimplementedState.remindDebtor(Long.parseLong("1")), messages);
    }

    @Test
    public void settleAllDebtTest() {
        assertEquals(unimplementedState.settleAllDebt("1"), messages);
    }

    @Test
    public void remindAllDebtTest() {
        assertEquals(unimplementedState.remindAllDebtor("1"), messages);
    }
}
