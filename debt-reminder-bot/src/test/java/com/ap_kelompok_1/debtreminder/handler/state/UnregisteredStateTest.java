package com.ap_kelompok_1.debtreminder.handler.state;

import com.ap_kelompok_1.debtreminder.controller.chat_source.ChatSource;
import com.ap_kelompok_1.debtreminder.handler.message.Messages;
import com.ap_kelompok_1.debtreminder.repository.LineUserRepository;
import com.linecorp.bot.model.message.Message;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UnregisteredStateTest {

    @Spy
    @InjectMocks
    private UnregisteredState unregisteredState;

    @Mock
    private List<Message> messages;

    @Mock
    private ChatSource chatSource;

    @Mock
    private LineUserRepository lineUserRepository;

    @Before
    public void setUp() throws ExecutionException, InterruptedException {
        when(lineUserRepository.isUserNameUnique("ipman")).thenReturn(true);
        when(lineUserRepository.isUserNameUnique("uciuci")).thenReturn(false);
    }

    @Test
    public void contextLoads() throws Exception {
        assertThat(unregisteredState).isNotNull();
    }

    @Test
    public void clearResponsesTest() {
        unregisteredState.clearResponses();
    }

    @Test
    public void registerTest() {
        messages.clear();
        messages.add(Messages.REGISTER_USERNAME_SPEC);
        assertEquals(unregisteredState.register("@#@#!cfr", "1", "1"),
            messages);
        messages.clear();
        messages.add(Messages.REGISTER_SUCCESS);
        assertEquals(unregisteredState.register("ipman", "1", "1")
            , messages);
        messages.clear();
        messages.add(Messages.REGISTER_TAKEN);
        assertTrue(unregisteredState.register("uciuci", "1", "1").equals(messages));
    }

    @Test
    public void profileTest() {
        messages.add(Messages.REGISTER_NOT);
        messages.add(Messages.REGISTER_USERNAME_SPEC);
        messages.clear();
        assertEquals(unregisteredState.profile("1", "1", "1", "1", "1"), messages);
    }

    @Test
    public void showDebtTest() {
        messages.add(Messages.REGISTER_NOT);
        messages.add(Messages.REGISTER_USERNAME_SPEC);
        messages.clear();
        assertEquals(unregisteredState.showDebt("1"), messages);
    }

    @Test
    public void showClaimTest() {
        messages.add(Messages.REGISTER_NOT);
        messages.add(Messages.REGISTER_USERNAME_SPEC);
        messages.clear();
        assertEquals(unregisteredState.showClaim("1"), messages);
    }

    @Test
    public void byeTest() {
        messages.add(Messages.REGISTER_NOT);
        messages.add(Messages.REGISTER_USERNAME_SPEC);
        messages.clear();
        assertEquals(unregisteredState.bye(new ChatSource(), "1"), messages);
    }

    @Test
    public void helpTest() {
        messages.add(Messages.REGISTER_NOT);
        messages.add(Messages.REGISTER_USERNAME_SPEC);
        messages.clear();
        assertEquals(unregisteredState.help("1"), messages);
    }

    @Test
    public void addDebteeTest() {
        messages.add(Messages.REGISTER_NOT);
        messages.add(Messages.REGISTER_USERNAME_SPEC);
        messages.clear();
        assertEquals(unregisteredState.addDebtee("1", "1"), messages);
    }

    @Test
    public void addAmountTest() {
        messages.add(Messages.REGISTER_NOT);
        messages.add(Messages.REGISTER_USERNAME_SPEC);
        messages.clear();
        assertEquals(unregisteredState.addAmount(Long.parseLong("1")), messages);
    }

    @Test
    public void addDueDateTest() {
        messages.add(Messages.REGISTER_NOT);
        messages.add(Messages.REGISTER_USERNAME_SPEC);
        messages.clear();
        assertEquals(unregisteredState.addDueDate(new Date()), messages);
    }

    @Test
    public void othersTest() {
        messages.add(Messages.REGISTER_NOT);
        messages.add(Messages.REGISTER_USERNAME_SPEC);
        messages.clear();
        assertEquals(unregisteredState.others("1", "1"), messages);
    }

    @Test
    public void cancelTest() {
        messages.add(Messages.REGISTER_NOT);
        messages.add(Messages.REGISTER_USERNAME_SPEC);
        messages.clear();
        assertEquals(unregisteredState.cancel("1"), messages);
    }

    @Test
    public void remindDebtorTest() {
        messages.add(Messages.REGISTER_NOT);
        messages.add(Messages.REGISTER_USERNAME_SPEC);
        messages.clear();
        assertEquals(unregisteredState.remindDebtor(Long.parseLong("1")), messages);
    }

    @Test
    public void remindAllDebtorTest() {
        messages.add(Messages.REGISTER_NOT);
        messages.add(Messages.REGISTER_USERNAME_SPEC);
        messages.clear();
        assertEquals(unregisteredState.remindAllDebtor("1"), messages);
    }

    @Test
    public void settleDebtTest() {
        messages.add(Messages.REGISTER_NOT);
        messages.add(Messages.REGISTER_USERNAME_SPEC);
        messages.clear();
        assertEquals(unregisteredState.settleDebt(Long.parseLong("1")), messages);
    }

    @Test
    public void settleAllDebtTest() {
        messages.add(Messages.REGISTER_NOT);
        messages.add(Messages.REGISTER_USERNAME_SPEC);
        messages.clear();
        assertEquals(unregisteredState.settleAllDebt("1"), messages);
    }

    @Test
    public void rejectTest() {
        messages.add(Messages.REGISTER_NOT);
        messages.add(Messages.REGISTER_USERNAME_SPEC);
        messages.clear();
        assertEquals(unregisteredState.reject(Long.parseLong("1")), messages);
    }

    @Test
    public void acceptTest() {
        messages.add(Messages.REGISTER_NOT);
        messages.add(Messages.REGISTER_USERNAME_SPEC);
        messages.clear();
        assertEquals(unregisteredState.accept(Long.parseLong("1")), messages);
    }
}
