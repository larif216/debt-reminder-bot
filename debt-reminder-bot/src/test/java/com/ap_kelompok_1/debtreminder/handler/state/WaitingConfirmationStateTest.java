package com.ap_kelompok_1.debtreminder.handler.state;

import org.junit.Test;

import static org.junit.Assert.*;

public class WaitingConfirmationStateTest {

    @Test
    public void clearResponses() {
    }

    @Test
    public void register() {
    }

    @Test
    public void profile() {
    }

    @Test
    public void showDebt() {
    }

    @Test
    public void showClaim() {
    }

    @Test
    public void bye() {
    }

    @Test
    public void help() {
    }

    @Test
    public void addDebtee() {
    }

    @Test
    public void addAmount() {
    }

    @Test
    public void addDueDate() {
    }

    @Test
    public void others() {
    }

    @Test
    public void cancel() {
    }

    @Test
    public void remindDebtor() {
    }

    @Test
    public void remindAllDebtor() {
    }

    @Test
    public void settleDebt() {
    }

    @Test
    public void settleAllDebt() {
    }

    @Test
    public void reject() {
    }

    @Test
    public void accept() {
    }
}