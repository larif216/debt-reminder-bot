package com.ap_kelompok_1.debtreminder.handler.state.helper;
import com.linecorp.bot.model.message.Message;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(MockitoJUnitRunner.class)
public class KursHelperTest {

    @InjectMocks
    KursHelper kursHelper;

    @Test
    public void contextLoads() throws Exception {
        assertThat(kursHelper).isNotNull();
    }

    @Test
    public void panggil(){
        List<Message> messages = new ArrayList<>();
        kursHelper.getPushMessages();
        kursHelper.getResponse();
        kursHelper.setResponses(messages);
    }
}
