package com.ap_kelompok_1.debtreminder.handler.state.helper;

import com.ap_kelompok_1.debtreminder.handler.message.Messages;
import com.ap_kelompok_1.debtreminder.handler.state.AddDebtAmountState;
import com.ap_kelompok_1.debtreminder.handler.state.AddDueDateState;
import com.ap_kelompok_1.debtreminder.handler.state.RegisteredState;
import com.ap_kelompok_1.debtreminder.handler.state.RemindAllDebtConfirmationState;
import com.ap_kelompok_1.debtreminder.handler.state.RemindDebtConfirmationState;
import com.ap_kelompok_1.debtreminder.handler.state.SettleAllDebtConfirmationState;
import com.ap_kelompok_1.debtreminder.handler.state.SettleDebtConfirmationState;
import com.ap_kelompok_1.debtreminder.handler.state.UnregisteredState;
import com.ap_kelompok_1.debtreminder.handler.state.WaitingConfirmationState;
import com.ap_kelompok_1.debtreminder.model.Debt;
import com.ap_kelompok_1.debtreminder.model.LineUser;
import com.ap_kelompok_1.debtreminder.repository.LineUserRepository;
import java.util.Date;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class StateHelperTest {
    @Spy
    @InjectMocks
    StateHelper stateHelper;

    @Mock
    LineUserRepository lineUserRepository;

    @Mock
    UnregisteredState unregisteredState;

    @Mock
    RegisteredState registeredState;

    @Test
    public void contextLoads() throws Exception {
        assertThat(stateHelper).isNotNull();
    }

    @Test
    public void panggil(){
        when(lineUserRepository.isUserRegistered("1")).thenReturn(true);
        when(lineUserRepository.isUserRegistered("2")).thenReturn(false);
        when(lineUserRepository.findStateById("1")).thenReturn(RegisteredState.DB_COL_NAME);
        doNothing().when(unregisteredState).clearResponses();
        doNothing().when(registeredState).clearResponses();

        stateHelper.toState("ASU");
        stateHelper.toState(AddDueDateState.DB_COL_NAME);
        stateHelper.toState(AddDebtAmountState.DB_COL_NAME);
        stateHelper.toState(RemindDebtConfirmationState.DB_COL_NAME);
        stateHelper.toState(RemindAllDebtConfirmationState.DB_COL_NAME);
        stateHelper.toState(SettleAllDebtConfirmationState.DB_COL_NAME);
        stateHelper.toState(SettleDebtConfirmationState.DB_COL_NAME);
        stateHelper.toState(RegisteredState.DB_COL_NAME);
        stateHelper.toState(UnregisteredState.DB_COL_NAME);
        stateHelper.toState(WaitingConfirmationState.DB_COL_NAME);

        stateHelper.getUserState("2");
        stateHelper.getUserState("1");
        assertEquals(stateHelper.getUserState("1"), registeredState);
        assertEquals(stateHelper.getUserState("2"), unregisteredState);
    }
}
