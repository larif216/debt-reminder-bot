package com.ap_kelompok_1.debtreminder.repository;

import com.ap_kelompok_1.debtreminder.model.Debt;
import com.ap_kelompok_1.debtreminder.model.LineUser;
import java.util.Date;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DebtRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DebtRepository debtRepository;

    @Test
    public void findDebtsByDebteeUserId_thenReturnDebts() {
        // given
        LineUser debtor = new LineUser("1", "ahmad", "ahmad");
        entityManager.persist(debtor);
        LineUser debtee = new LineUser("2", "zaky", "zaky");
        entityManager.persist(debtee);
        Debt debt = new Debt(debtee, debtor, 10000L, new Date());
        entityManager.merge(debt);
        entityManager.flush();

        List<Debt> found = debtRepository.findDebtsByDebteeUserId(debtee.getUserId());

        assertThat(found.get(0).getDebtAmount())
            .isEqualTo(debt.getDebtAmount());
    }

    @Test
    public void findDebtsByDebtorUserId_thenReturnDebts() {
        // given
        LineUser debtor = new LineUser("1", "ahmad", "ahmad");
        entityManager.persist(debtor);
        LineUser debtee = new LineUser("2", "zaky", "zaky");
        entityManager.persist(debtee);
        Debt debt = new Debt(debtee, debtor, 10000L, new Date());
        entityManager.merge(debt);
        entityManager.flush();

        List<Debt> found = debtRepository.findDebtsByDebtorUserId(debtor.getUserId());

        assertThat(found.get(0).getDebtAmount())
            .isEqualTo(debt.getDebtAmount());
    }
}
