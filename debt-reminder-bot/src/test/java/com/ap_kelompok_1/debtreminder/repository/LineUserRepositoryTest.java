package com.ap_kelompok_1.debtreminder.repository;

import com.ap_kelompok_1.debtreminder.model.LineUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class LineUserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private LineUserRepository lineUserRepository;

    @Test
    public void findLineByUserId_thenReturLineUser() {
        LineUser lineUser = new LineUser("1", "ahmad", "ahmad");
        entityManager.persist(lineUser);
        entityManager.flush();

        LineUser found = lineUserRepository.findLineUserByUserId(lineUser.getUserId());

        assertThat(found.getUserName())
            .isEqualTo(lineUser.getUserName());
    }

    @Test
    public void findLineByUserName_thenReturLineUser() {
        LineUser lineUser = new LineUser("1", "ahmad", "ahmad");
        entityManager.persist(lineUser);
        entityManager.flush();

        LineUser found = lineUserRepository.findLineUserByUserName(lineUser.getUserName());

        assertThat(found.getUserName())
            .isEqualTo(lineUser.getUserName());
    }

    //    @Query(value = "SELECT * FROM LINE_USER WHERE USER_ID = ?1", nativeQuery = true)
    //    LineUser findLineByUserId(String userID);
    //
    //    @Query(value = "SELECT * FROM LINE_USER WHERE USER_NAME = ?1", nativeQuery = true)
    //    LineUser findLineUserByUserName(String name);
}
